#include "MyHockeyist.h"

#ifndef MYSTANDARTHOCKEYIST_H
#define MYSTANDARTHOCKEYIST_H

class MyStandartHockeyist : public MyHockeyist
{
public:
    MyStandartHockeyist() : MyHockeyist() {};
    MyStandartHockeyist(const model::Hockeyist& other) : MyHockeyist(other) {};

    bool takePuckToDefense(const model::World &world, const model::Game &game, model::Move &move) const;
    void moveToPuck(const model::World &world, const model::Game &game, model::Move &move);
    void defenseGoalNet(const model::World &world, const model::Game &game, model::Move &move);
    void moveToGoalNet(const model::World &world, const model::Game &game, model::Move &move);
    void strikeToGoalNet(const model::World &world, const model::Game &game, model::Move &move) const;
    bool solveTroubles(const model::World &world, const model::Game &game, model::Move &move);
};

#endif // MYSTANDARTHOCKEYIST_H
