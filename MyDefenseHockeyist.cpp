#include "MyDefenseHockeyist.h"
#include "MyStandartHockeyist.h"
#include "StrategyMethods.h"

using namespace MyStrategyConst;
using namespace MyHockeyistConst;
using namespace std;
using namespace model;
using namespace StrategyMethods;

bool MyDefenseHockeyist::takePuckToDefense(const model::World &world, const model::Game &game, model::Move &move) const
{
    MyStandartHockeyist self2{*this};
    return self2.takePuckToDefense(world, game, move);
    const Puck& puck = world.getPuck();
    if (getDistanceTo(puck) < game.getStickLength() && fabs(getAngleTo(puck)) < game.getStickSector()/2.0)
    {
        if (puck.getOwnerHockeyistId() != -1)
        {
            move.setAction(ActionType::STRIKE);
        }
        else
        {
            for (auto hockeyist : world.getHockeyists())
            {
               if (isActiveHockeyist(hockeyist, true) && hockeyist.getId() != getId()
                       && hockeyist.getDistanceTo(getGoalnetDefensePlace(world)) < MAX_DISTANCE_TO_SPEED_UP
                       && hockeyist.getRemainingCooldownTicks() == 0
                       && hockeyist.getRemainingKnockdownTicks() == 0)
                {
                    move.setAction(ActionType::TAKE_PUCK);
                    return true;
                }
            }
            if (world.getPuck().getSpeedY() > game.getGoalieMaxSpeed()
                    && fabs(getAngleTo(world.getMyPlayer().getNetFront(), MIDDLE_BY_Y)) > MAX_ANGLE_TO_SPEED_UP)
            {
                move.setAction(ActionType::STRIKE);
            }
            else
            {
                move.setAction(ActionType::TAKE_PUCK);
            }
        }
        return true;
    }
    return false;
}

void MyDefenseHockeyist::moveToPuck(const model::World &world, const model::Game &game, model::Move &move)
{
    if (!takePuckToDefense(world, game, move))
    {
        Point puckPlace = getPuckBestPlace(*this, world, game, 0, 0, 0, 0);
        {
            for (auto hockeyist : world.getHockeyists())
            {
                if (isActiveHockeyist(hockeyist, true) && hockeyist.getId() != getId()
                        && hockeyist.getType() != HockeyistType::FORWARD
                        && hockeyist.getDistanceTo(getGoalnetDefensePlace(world)) < getDistanceTo(getGoalnetDefensePlace(world))
                        && hockeyist.getId() != world.getPuck().getOwnerHockeyistId()
                        && fabs(puckPlace.getX() - world.getMyPlayer().getNetFront()) < MIDDLE_BY_X
                        )
                {
                    MyStandartHockeyist self2{*this};
                    self2.moveToPuck(world, game, move);
                    return;
                }
            }
            defenseGoalNet(world, game, move);
        }
//        moveTo(*this, world, game, move, getGoalnetDefensePlace(*this, world), puck, true);
    }
}

void MyDefenseHockeyist::defenseGoalNet(const model::World &world, const model::Game &game, model::Move &move)
{
    if (takePuckToDefense(world, game, move))
    {
        return;
    }
    if (world.getPuck().getOwnerPlayerId() == world.getMyPlayer().getId())
    {
        for (auto hockeyist : world.getHockeyists())
        {
            if (isActiveHockeyist(hockeyist, false)
                    && fabs(hockeyist.getX() - world.getMyPlayer().getNetFront()) < fabs(getX() - world.getMyPlayer().getNetFront())
                    && fabs(getX() - world.getMyPlayer().getNetFront()) < MIDDLE_BY_X)
            {
//                moveTo(*this, world, game, move, hockeyist, false);
                if (getDistanceTo(hockeyist) < game.getStickLength()
                        && fabs(getAngleTo(hockeyist)) < game.getStickSector()/2.0)
                {
                    move.setAction(ActionType::STRIKE);
                }
                return;
            }
        }
    }
    if (world.getHockeyists().size() == 6)
    {
        Point aim = getPuckBestPlace(*this, world, game);
        if (fabs(aim.getX() - world.getMyPlayer().getNetFront()) < DISTANSE_FROM_MY_GOALNET_X)
        {
            moveTo(*this, world, game, move, aim, 1);
        }
        for (auto hockeyist : world.getHockeyists())
        {
            if (isActiveHockeyist(hockeyist, true) && hockeyist.getId() != getId()
                    && hockeyist.getId() != world.getPuck().getOwnerHockeyistId()
                    && hockeyist.getDistanceTo(getGoalnetDefensePlace(world)) < getDistanceTo(getGoalnetDefensePlace(world)))
            {
                Point startPos = getGoalnetDefensePlace(world);
                Point aim( (startPos.getX() > MIDDLE_BY_X ? startPos.getX() - DISTANSE_FROM_MY_GOALNET_X : startPos.getX() + DISTANSE_FROM_MY_GOALNET_X),
                        startPos.getY());
                moveTo(*this, world, game, move, getAttackPlace(*this, world, game), world.getPuck(), true);
                return;
            }
        }
    }
    for (auto hockeyist : world.getHockeyists())
    {
        if (isActiveHockeyist(hockeyist, true) && hockeyist.getId() != Singleton::getInstance().getHockeyistIdMinTime()
                && fabs(world.getPuck().getX() - world.getMyPlayer().getNetFront()) > MIDDLE_BY_X
                && hockeyist.getStamina() > STAMINA_CHANGE_COEF_IN_GAME * STAMINA_MAX_HOCKEYIST
                && getStamina() < hockeyist.getStamina()
                && getStamina() < STAMINA_CHANGE_COEF_IN_GAME * STAMINA_MAX_HOCKEYIST)
        {
            moveToSubstitute(*this, world, game, move);
            return;
        }
    }
    moveTo(*this, world, game, move, getGoalnetDefensePlace(world), world.getPuck(), true);
//    moveTo(*this, world, game, move, getGoalnetDefensePlace(world), Point(world.getMyPlayer().getNetFront(), MIDDLE_BY_Y), true);
}

void MyDefenseHockeyist::moveToGoalNet(const model::World &world, const model::Game &game, model::Move &move)
{
/*    if (world.getPuck().getOwnerHockeyistId() == getId())
    {
        solveTroubles(world, game, move);
        if (move.getAction() == ActionType::PASS)
        {
            return;
        }
    }*/
    MyStandartHockeyist standartSelf{*this};
    standartSelf.moveToGoalNet(world, game, move);
}

void MyDefenseHockeyist::strikeToGoalNet(const model::World &world, const model::Game &game, model::Move &move) const
{
    MyStandartHockeyist standartSelf{*this};
    standartSelf.strikeToGoalNet(world, game, move);
}

bool MyDefenseHockeyist::solveTroubles(const model::World &world, const model::Game &game, model::Move &move)
{
    MyStandartHockeyist standartHockeyist{*this};
    return standartHockeyist.solveTroubles(world, game, move);
}
