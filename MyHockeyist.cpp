#include "MyHockeyist.h"
#include "MyStrategyConst.h"
#include "StrategyMethods.h"

using namespace model;
using namespace std;
using namespace MyStrategyConst;
using namespace StrategyMethods;

#include <cmath>
#include <iostream>


 void MyHockeyist::aimTo(const MyHockeyist &self, const model::World &world, const model::Game &game, model::Move &move, const Point &aim)
{
    double angleToUnit = self.getAngleTo(aim.getX(), aim.getY());
    move.setTurn(angleToUnit);
}

// 0 - move Forward
// 1 - can move Back
// 2 - move Back
 void MyHockeyist::moveTo(const MyHockeyist &self, const model::World &world, const model::Game &game, model::Move &move, const Point &aim, int backMove)
{
    if (self.getDistanceTo(aim) < HOCKEYIST_RADIUS/4.0)
    {
//        print_debug(cout, world.getTick(), self.getId(), "Nothing");
        return;
    }
    Vec2d opt_dir = (Vec2d(aim.getX()-self.getX(), aim.getY() - self.getY()))
//            *game.getHockeyistMaxSpeed()
            - (Vec2d(self.getSpeedX(), self.getSpeedY()))
/*             * (getSpeed(self) > MIN_SPEED_TO_SPEED_DOWN ?
             getSpeed(self) /
                     (((fabs(self.getAngleTo(self.getX() + self.getSpeedX(), self.getY() + self.getSpeedY()))
                             > MAX_ANGLE_TO_SPEED_UP
                             ? game.getHockeyistSpeedUpFactor() :
                             game.getHockeyistSpeedDownFactor()) + getSpeed(self) * BRAKE_DOWN_COEF_HOCKEY) * self.getHockeyistSpeedDereviation())
            : 1.0))*/
    ;
    double angle = self.getAngleTo(self.getX() + opt_dir.getX(), self.getY() + opt_dir.getY());
    bool nowBack = false;
    if ((backMove == 1
            && (timeTo(self, game, aim, true) < timeTo(self, game, aim, false)))
//        || backMove == 2
        || (aim.getX() == getGoalnetDefensePlace(world).getX() && aim.getY() == getGoalnetDefensePlace(world).getY()
                && fabs(self.getX() - world.getMyPlayer().getNetFront()) > fabs(aim.getX() - world.getMyPlayer().getNetFront()))
            )
    {
        nowBack = true;
        angle -= M_PI;
        setCorrectAngle(angle);
        move.setTurn(angle);
        move.setSpeedUp(fabs(angle) < MAX_ANGLE_TO_SPEED_UP ? DEFAULT_BRAKE_DOWN : DEFAULT_SPEED_UP);
    }
    else
    {
        move.setTurn(angle);
        move.setSpeedUp(fabs(angle) < MAX_ANGLE_TO_SPEED_UP ? DEFAULT_SPEED_UP : DEFAULT_BRAKE_DOWN);
    }
    if (fabs(angle) > MAX_ANGLE_TO_SPEED_UP
            || fabs(world.getMyPlayer().getNetFront() - self.getX()) > MIDDLE_BY_X)
    {
        return;
    }
    if (getSpeed(self) == 0)
    {
        return;
    }
    double n = self.getDistanceTo(aim) / getSpeed(self);
    if ((getSpeed(self) -
            n * (nowBack ? game.getHockeyistSpeedUpFactor() : game.getHockeyistSpeedDownFactor())) * self.getHockeyistSpeedDereviation()
            * pow(BRAKE_DOWN_COEF_HOCKEY, n)
            > MIN_SPEED_TO_SPEED_DOWN)
    {
        move.setSpeedUp(nowBack ? DEFAULT_SPEED_UP : DEFAULT_BRAKE_DOWN);
    }
/*    if (fabs(angle) < MAX_ANGLE_TO_SPEED_UP)
    {
        double n = timeTo(self, game, aim, (move.getSpeedUp() == DEFAULT_BRAKE_DOWN));
        double v = getSpeed(self);
        int tickToDown = 0;
        while (v > 0.5)
        {
            tickToDown++;
            v = (v - (move.getSpeedUp() == DEFAULT_BRAKE_DOWN ? game.getHockeyistSpeedUpFactor() : game.getHockeyistSpeedDownFactor())
                    * self.getHockeyistSpeedDereviation()) * BRAKE_DOWN_COEF_HOCKEY;
        }
        if (tickToDown > n)
        {
            move.setSpeedUp((move.getSpeedUp() == DEFAULT_BRAKE_DOWN ? DEFAULT_SPEED_UP : DEFAULT_BRAKE_DOWN));
        }
    }*/
/*     if (getSpeed(self) / ((game.getHockeyistSpeedUpFactor() + BRAKE_DOWN_COEF_HOCKEY * getSpeed(self)) * self.getHockeyistSpeedDereviation())
             > self.getDistanceTo(aim) / getSpeed(self))
     {
         if (self.getAngleTo(self.getX() + self.getSpeedX(), self.getY() + self.getSpeedY())
                 < MAX_ANGLE_TO_SPEED_UP)
         {
             move.setSpeedUp(DEFAULT_BRAKE_DOWN);
         }
         else
         {
             move.setSpeedUp(DEFAULT_SPEED_UP);
         }
     }*/
 }

 void MyHockeyist::moveTo(const MyHockeyist &self, const model::World &world, const model::Game &game, model::Move &move, const Point &aim, const Point &aim2, bool backMove)
{
/*     else if (backMove)
     {
         double futureAngle = self.getAngle() + self.getAngleTo(aim);
         while (futureAngle > M_PI) { futureAngle -= 2 * M_PI; }
         while (futureAngle < -M_PI) { futureAngle += 2 * M_PI; }
         Point futureSelf(self.getId(), self.getMass(), self.getRadius(), aim.getX(), aim.getY(), 0, 0, futureAngle, self.getAngularSpeed());
         Point futureSelfBack(self.getId(), self.getMass(), self.getRadius(), aim.getX(), aim.getY(), 0, 0, self.getAngle(), self.getAngularSpeed());
         if (fabs(futureSelfBack.getAngleTo(aim2)) < fabs(futureSelf.getAngleTo(aim2))
                 && fabs(self.getAngleTo(aim)) > MAX_ANGLE_TO_SPEED_UP)
         {
             move.setSpeedUp(DEFAULT_BRAKE_DOWN);
             double angleTurn = self.getAngleTo(aim);
             angleTurn -= M_PI;
             while (angleTurn > M_PI) { angleTurn -= 2 * M_PI; }
             while (angleTurn < -M_PI) { angleTurn += 2 * M_PI; }
             move.setTurn(angleTurn);
         }
     }*/

/*     if (backMove)
     {
         double selfAngle = self.getAngle();
         Point futureSelf(self.getId(), self.getRadius(), self.getMass(), aim.getX(), aim.getY(),
         self.getSpeedX(), self.getSpeedY(), self.getAngle(), self.getAngularSpeed());
         selfAngle -= M_PI;
         Point futureSelfBack(self.getId(), self.getRadius(), self.getMass(), aim.getX(), aim.getY(),
                 self.getSpeedX(), self.getSpeedY(), getCorrectAngle(selfAngle), self.getAngularSpeed());
         if (getCorrectAngle(fabs(futureSelfBack.getAngleTo(aim2))) + M_PI/6 < getCorrectAngle(fabs(futureSelf.getAngleTo(aim2))))
         {
             if (getCorrectAngle(fabs(self.getAngleTo(aim))) >= MAX_ANGLE_TO_SPEED_UP)
             {
                 moveTo(self, world, game, move, aim, 0);
                 move.setTurn(self.getAngleTo(aim));
             }
             else
             {
                 moveTo(self, world,game, move, aim, 2);
                 move.setTurn(getCorrectAngle(self.getAngleTo(aim) - M_PI));
             }
         }
         else
         {
             if (getCorrectAngle((self.getAngleTo(aim))) >= MAX_ANGLE_TO_SPEED_UP)
             {
                 moveTo(self, world, game, move, aim, 2);
                 move.setTurn(getCorrectAngle(self.getAngleTo(aim) - M_PI));
             }
             else
             {
                 moveTo(self, world,game, move, aim, 0);
                 move.setTurn(getCorrectAngle(self.getAngleTo(aim)));
             }
         }
         print_debug(cout, world.getTick(), self.getId(), aim.getX(), aim.getY(), self.getAngleTo(aim), aim2.getX(), aim2.getY(), self.getAngleTo(aim2),
                 "Speed Up: ", move.getSpeedUp(), move.getTurn(),
                 "Angles: ", futureSelfBack.getAngleTo(aim2), futureSelf.getAngleTo(aim2));
     }
     else
     {
         moveTo(self, world, game, move, aim, 0);
     }*/

     moveTo(self, world, game, move, aim, backMove ? 1 : 0);
    if ( self.getDistanceTo(aim) < MAX_DISTANCE_TO_SPEED_UP)
     {
         aimTo(self, world, game, move, aim2);
     }
}

void MyHockeyist::moveToSubstitute(const MyHockeyist &self, const model::World &world, const model::Game &game, model::Move &move)
{
    if (fabs(self.getAngleTo(world.getPuck())) < game.getStickSector()/2.0
        && self.getDistanceTo(world.getPuck()) < game.getStickLength()
        && world.getPuck().getOwnerPlayerId() != world.getMyPlayer().getId()
    )
    {
        move.setAction(TAKE_PUCK);
        return;
    }
    Point aim((world.getMyPlayer().getNetFront() + MIDDLE_BY_X)/2.0, BORDER_TOP_Y);
    MyHockeyist::moveTo(self, world, game, move, aim, 1);
    int teammateIndex = substituteIndex(self, world, game);
    if (teammateIndex != -1)
    {
        move.setAction(ActionType::SUBSTITUTE);
        move.setTeammateIndex(teammateIndex);
    }
}

double MyHockeyist::getPuckAngleDereviation() const
{
    return 100.0/ getOnStamina(getDexterity());
}

double MyHockeyist::getPuckSpeedDereviation() const
{
    return getOnStamina(getStrength())/100.0;
}

double MyHockeyist::getOnStamina(double attribute) const
{
    return attribute * (STAMINA_MIN_COEF + STAMINA_ADD_COEF * getStamina() / STAMINA_MAX_HOCKEYIST);
}

double MyHockeyist::getHockeyistTurnDereviation() const
{
    return getOnStamina(getAgility())/100.0;
}

double MyHockeyist::getHockeyistSpeedDereviation() const
{
    return getOnStamina(getAgility())/100.0;
}
