#include "MyStrategy.h"
#include "MyStandartHockeyist.h"
#include "MyDefenseHockeyist.h"
#include "MyAgressiveHockeyist.h"
#include "MyHockeyist.h"
#include "StrategyMethods.h"
//extern "C" {
//    #include <SDL2/SDL.h>
//}
#include <cstdio>

#define M_PI 3.14159265358979323846
#define _USE_MATH_DEFINES

#include <cmath>
#include <cstdlib>

using namespace model;
using namespace std;
using namespace MyStrategyConst;

void MyStrategy::move(MyHockeyist &self, const model::World &world, const model::Game &game, model::Move &move)
{
    if (world.getMyPlayer().isJustMissedGoal() || world.getMyPlayer().isJustScoredGoal())
    {
        if (self.getStamina() < STAMINA_CHANGE_COEF_AFTER_GOAL * game.getHockeyistMaxStamina())
        {
            MyHockeyist::moveToSubstitute(self, world, game, move);
        }
        else
        {
            Point target;
            double minStamina = STAMINA_MAX_HOCKEYIST;
            for (auto hockeyist : world.getHockeyists())
            {
                if (StrategyMethods::isActiveHockeyist(hockeyist, false)
                        && (hockeyist.getStamina() < minStamina)
                        )
                {
                    target = hockeyist;
                }
            }
            if (target.getX() != 0)
            {
                MyHockeyist::moveTo(self, world, game, move, target, 0);
                if (self.getDistanceTo(target) < game.getStickLength()
                        && fabs(self.getAngleTo(target)) < game.getStickSector()/2.0)
                {
                    move.setAction(ActionType::STRIKE);
                }
            }
        }
        if (world.getTick()%100 == 0)
        {
//            StrategyMethods::print_debug(cout, world.getTick(), world.getMyPlayer().getGoalCount(), ":", world.getOpponentPlayer().getGoalCount());
        }
        return;
    }
//    fout.open("output2.txt", std::ofstream::out | std::ofstream::app);
/*    if (world.getTick() == 0)
    {
        for (double x = BORDER_LEFT_X + HOCKEYIST_RADIUS + BINDING_PUCK_DISTANCE; x < MIDDLE_BY_X + 200; x += 1)
        {
            for (double y = BORDER_TOP_Y + HOCKEYIST_RADIUS; y < world.getOpponentPlayer().getNetTop(); y += 1)
            {
                pair<double, double> passRange(M_PI + 1, -M_PI - 1);
                pair<double, double> strikeRange(M_PI + 1, -M_PI - 1);
                for (double angleSelf = M_PI/2; angleSelf < M_PI; angleSelf += M_PI/180.0)
                {
                    if (StrategyMethods::isStrikeGetGoal(MyStandartHockeyist(Hockeyist(0, 0, 0, self.getMass(), self.getRadius(), x, y, 0, 0, angleSelf, 0, true,
                            HockeyistType::VERSATILE, 0, 0, 0, 0, 0, HockeyistState::SWINGING,
                            0, 0, 0, game.getMaxEffectiveSwingTicks(), ActionType::NONE, 0)), world, game, false))
                    {
//                    StrategyMethods::print_debug(fout, "GOOD: ", x, y, angleSelf * 180 / M_PI,
//                            x + cos(angleSelf) * (550 - y) / sin(angleSelf));
                        strikeRange.first = min(angleSelf, strikeRange.first);
                        strikeRange.second = max(angleSelf, strikeRange.second);
//                    StrategyMethods::print_debug(fout, x, y, strikeRange.first, strikeRange.second, passRange.first, passRange.second);
                    }
                    if (
                            StrategyMethods::isStrikeGetGoal(MyStandartHockeyist(Hockeyist(0, 0, 0, 0, 0, x, y, 0, 0, angleSelf, 0, true,
                                    HockeyistType::VERSATILE, 0, 0, 0, 0, 0, HockeyistState::ACTIVE,
                                    0, 0, 0, 0, ActionType::NONE, 0)), world, game, false, 0)
                                    && StrategyMethods::isStrikeGetGoal(MyStandartHockeyist(Hockeyist(0, 0, 0, 0, 0, x, y, 0, 0, angleSelf, 0, true,
                                    HockeyistType::VERSATILE, 0, 0, 0, 0, 0, HockeyistState::ACTIVE,
                                    0, 0, 0, 0, ActionType::NONE, 0)), world, game, false, -game.getPassAngleDeviation())
                                    && StrategyMethods::isStrikeGetGoal(MyStandartHockeyist(Hockeyist(0, 0, 0, 0, 0, x, y, 0, 0, angleSelf, 0, true,
                                    HockeyistType::VERSATILE, 0, 0, 0, 0, 0, HockeyistState::ACTIVE,
                                    0, 0, 0, 0, ActionType::NONE, 0)), world, game, false, game.getPassAngleDeviation())
                            )
                    {
                        passRange.first = min(angleSelf, passRange.first);
                        passRange.second = max(angleSelf, passRange.second);
                    }
                }
                if (passRange.first != M_PI + 1 || strikeRange.first != M_PI + 1)
                {
                    StrategyMethods::print_debug(fout, x, y, strikeRange.first, strikeRange.second, passRange.first, passRange.second);
                }
            }
            StrategyMethods::print_debug(cout, x);
        }
    }*/
    const Puck puck = world.getPuck();
    if (self.getState() == SWINGING)
    {
        self.strikeToGoalNet(world, game, move);
/*        if (self.getSwingTicks() > game.getMaxEffectiveSwingTicks())
        {
            if (move.getAction() != STRIKE || move.getAction() != CANCEL_STRIKE)
            {
                move.setAction(STRIKE);
            }
        }*/
        return;
    }
    if (puck.getOwnerPlayerId() == world.getMyPlayer().getId())
    {
        if (StrategyMethods::isInBigTroubles(self, world, game))
        {
            if (self.solveTroubles(world, game, move))
            {
                return;
            }
        }
        if (puck.getOwnerHockeyistId() == self.getId())
        {
            self.moveToGoalNet(world, game, move);
        }
        else
        {
            self.defenseGoalNet(world, game, move);
        }
    }
    else
    {
//            StrategyMethods::print_debug(cout, world.getTick(), "Puck Now: ", puck.getX(), puck.getY(), puck.getSpeedX(), puck.getSpeedY(),
//                    self.getAngle(), sqrt(StrategyMethods::sqr(puck.getSpeedX()) + StrategyMethods::sqr(puck.getSpeedY())));
        self.moveToPuck(world, game, move);
    }
    if (move.getAction() == PASS)
    {
//        StrategyMethods::print_debug(cout, world.getTick(), "PASS: ", StrategyMethods::isStrikeGetGoal(self, world, game, false, move.getPassAngle(), 0, false));
    }
//    fout.close();

}


void MyStrategy::move(const Hockeyist& self2, const World& world, const Game& game, Move& move)
{
//    StrategyMethods::print_debug(cout, world.getTick());
//    cerr<<game.getStrikeAngleDeviation()<<"\n";
    Singleton::getInstance().updateInformation(world, game);
    if (world.getHockeyists().size() > 6)
    {
        if (self2.getState() == HockeyistState::RESTING)
        {
            return;
        }
        if (self2.getType() == HockeyistType::RANDOM)
        {
            MyStandartHockeyist self{self2};
            MyStrategy::move(self, world, game, move);
        }
        else if  (self2.getType() == HockeyistType::VERSATILE)
        {
            MyStandartHockeyist self{self2};
            MyStrategy::move(self, world, game, move);
        }
        else if (self2.getType() == HockeyistType::FORWARD)
        {
            MyStandartHockeyist self{self2};
            MyStrategy::move(self, world, game, move);
        }
        else
        {
            MyStandartHockeyist self{self2};
            MyStrategy::move(self, world, game, move);
        }
        return;
    }
    if (world.getMyPlayer().getGoalCount() < world.getOpponentPlayer().getGoalCount()
            || (world.getMyPlayer().getGoalCount() == world.getOpponentPlayer().getGoalCount()
            && world.getTick() > world.getTickCount() - TICKS_FOR_AGRESSIVE)
            )
    {
        MyStandartHockeyist self{self2};
        MyStrategy::move(self, world, game, move);
    }
    else if (world.getMyPlayer().getGoalCount() <= world.getOpponentPlayer().getGoalCount() + 1)
    {
        MyStandartHockeyist self{self2};
        MyStrategy::move(self, world, game, move);
    }
    else
    {
        MyStandartHockeyist self{self2};
        MyStrategy::move(self, world, game, move);
    }
}

MyStrategy::MyStrategy() { }

