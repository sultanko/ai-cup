#ifndef _VEC2D_H_
#define _VEC2D_H_

#include <cmath>
#include <ostream>
#include <iostream>
#include "Point.h"

class Vec2d
{
    double x;
    double y;
public:
    Vec2d() : x(0), y(0) {};

    Vec2d(double kx, double ky) : x(kx), y(ky) {};

    Vec2d(const Vec2d &vec) : x(vec.x), y(vec.y) {};

    Vec2d(const Point& point) : x(point.getX()), y(point.getY()) {};

    double getX() const
    {
        return x;
    }

    double getY() const
    {
        return y;
    }

    void setX(double nx)
    {
        x = nx;
    }

    void setY(double ny)
    {
        y = ny;
    }

    double length() const
    {
        return sqrt(x * x + y * y);
    }

    Vec2d normalize() const
    {
        return Vec2d(x / length(), y / length());
    }

    Vec2d operator+(const Vec2d &b) const
    {
        return Vec2d(x + b.x, y + b.y);
    }

    Vec2d operator-(const Vec2d &b) const
    {
        return Vec2d(x - b.x, y - b.y);
    }

    Vec2d operator-() const
    {
        return Vec2d(-x, -y);
    }


    Vec2d operator*(double rhs)
    {
        return Vec2d(x * rhs, y * rhs);
    }

    double operator*(const Vec2d& rhs) const
    {
        return x * rhs.x + y * rhs.y;
    }

    ~Vec2d() {};

};


#endif