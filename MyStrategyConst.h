#ifndef _MY_STRATEGY_CONST_H_
#define _MY_STRATEGY_CONST_H_

#define M_PI 3.14159265358979323846
#define _USE_MATH_DEFINES

#include <cmath>

namespace MyStrategyConst {
    const double MIN_ANGLE_TO_TURN = M_PI/90.0;
    const double MIN_DISTANCE_TO_MOVE = 25;
    const double DELTA_FOR_ANGLE_VARIANTS = M_PI/180.0;
    const double MAX_DISTANCE_TO_SPEED_UP = 75;
    const double INF = 100500;
    const double MIN_PUCK_SPEED_FOR_BRUTEFORCE = 1.0;
    const double DEFAULT_SPEED_UP = 1.0;
    const double DEFAULT_BRAKE_DOWN = -1.0;
    const double DEFAULT_TURN_ANGLE = M_PI/90.0;
    const double MAX_ANGLE_TO_SPEED_UP = M_PI/2.0;
    const double DISTANCE_TO_GOALNET_X = 220; // very good 275 : update 400
    const double DISTANCE_TO_GOALNET_Y = 30; // by lineika 70 + eye : update 180
    const double DISTANCE_TO_STRIKE_CORNER_X = 10;
    const double COEF_VELOCITY = 10;
    const double MIN_SPEED_TO_SPEED_DOWN = 0.2;
    const double MAX_TICKS_FOR_TURN = 10;
    const int TICKS_FOR_PUCK = 250;
    const double MAX_ANGLE_TO_TAKE_PASS = M_PI/3.0;
    const double DEFAULT_PASS_POWER = 1.0;
    const double BORDER_LEFT_X = 65;
    const double BORDER_RIGHT_X = 1135;
    const double BORDER_BOTTOM_Y = 770;
    const double BORDER_RIGHT_BACK_X = 1200;
    const double BORDER_LEFT_BACK_X = 0;
    const double BORDER_TOP_Y = 150;
    const double BORDER_GOALNET_TOP_Y = 360;
    const double BORDER_GOALNET_BOTTOM_Y = 560;
    const double DISTANSE_FROM_MY_GOALNET_X = 100;
    const double MAX_DISTANCE_TO_MOVE_BACK = 300;
    const double DISTANCE_FOR_FLANK_Y = 10;
    const double DISTANCE_FOR_FLANK_X = 100;
    const double MIDDLE_BY_X = (BORDER_RIGHT_X + BORDER_LEFT_X)/2.0;
    const double MIDDLE_BY_Y = (BORDER_TOP_Y + BORDER_BOTTOM_Y)/2.0;
    const double GAME_SPEED_DOWN_FACTOR = 0.02;
    const double MIN_ANGLE_TO_MY_GOALNET = M_PI/4;
    const int TICKS_FOR_GOAL_PUCK = 120;
    const double HOCKEYIST_RADIUS = 30;
    const double PUCK_RADIUS = 20;
    const double BRAKE_DOWN_COEF = 0.999;
    const double BRAKE_DOWN_COEF_HOCKEY = 0.98;
    const double DELTA_FOR_STRIKE = M_PI/2.0;
    const int TICKS_FOR_SECOND_AIM = 30;
    const double MAX_ANGLE_TO_SECOND_AIM = M_PI/36;
    const double WALL_BRAKE_COEF = -0.25;
    const double WALL_GOALNET_BRAKE_COEF = -0.1;
    const double PENETRATION_COEF = 0.2;
    const double BRAKE_ANGLE_COEF = 0.999;
    const int TICKS_FOR_AGRESSIVE = 1000;
    const double BINDING_PUCK_DISTANCE = 55;
    const double ANGULAR_SPEED_BRAKE_DOWN_COEF = 0.0270190131;
    const double STRIKE_ANGLE_DERIVIATION_COEF = 1;
    const double MAX_AVOID_DISTANCE = 2 * HOCKEYIST_RADIUS;
    const double MAX_AVOID_DISTANCE_WITH_PUCK = MAX_AVOID_DISTANCE + BINDING_PUCK_DISTANCE;
    const double PASS_PUCK_SPEED = 10;
    const double MIN_STRIKE_PROBABILILITY = 0.95;
    const double MIN_PASS_PROBABILITY = 0.95;
    const double MIN_SWINGING_STRIKE_PROBABILITY = 0.9;
    const double MIN_STRIKE_PANIC_PROBABILITY = 0.9;
    const double STRIKE_COEF = 0.2;
    const double STRIKE_DERIVIATION_DELTA = M_PI/180.0/2.0/2.0;
    const double MIN_SPEED_FOR_STRIKE = 14.0;
    const double MAX_ANGLE_DEREVIATION_FOR_STRIKE = 4.0 * M_PI/180.0;
    const double SPEED_UP_HOCKEYIST_FACTOR = 12500.0/60.0/60.0/30.0;
    const double SPEED_DOWN_HOCKEYIST_FACTOR = -7500.0/60.0/60.0/30.0;
    const double STAMINA_CHANGE_COEF_AFTER_GOAL = 1800/2000.0;
    const double STAMINA_CHANGE_COEF_IN_GAME = 1250.0/2000.0;
    const double STAMINA_CHANGE_COEF_DEFENCEMAN = 1500.0/2000.0;
    const double STAMINA_MIN_COEF = 0.75;
    const double STAMINA_ADD_COEF = 0.25;
    const double STAMINA_MAX_HOCKEYIST = 2000;
    const double STAMINA_MIN_DELTA = 100;
    const double GOALIE_MAX_SPEED = 6;
    const double MAX_PUCK_BRUTFORCE_X = 150;
    const double CHANCE_PUCK_STRIKE_GOALIE = 0.4;
    const double CHANCE_PUCK_STRIKE_WALL = 0.1;
    const double ENABLED_DEFORMATION = 0.05;
}

namespace MyHockeyistConst {
    enum class TacticState {
        defense,
        attack,
        neitral
    };
}

#endif
