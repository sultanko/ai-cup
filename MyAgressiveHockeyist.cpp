#include "MyAgressiveHockeyist.h"
#include "StrategyMethods.h"
#include "MyStrategyConst.h"
#include "MyStandartHockeyist.h"
#include <iostream>

using namespace std;
using namespace StrategyMethods;
using namespace MyStrategyConst;
using namespace model;

bool MyAgressiveHockeyist::takePuckToDefense(const World &world, const Game &game, model::Move &move) const
{
    MyStandartHockeyist self2{*this};
    return self2.takePuckToDefense(world, game, move);
    const Puck& puck = world.getPuck();
    for (auto hockeyist : world.getHockeyists())
    {
        if (isActiveHockeyist(hockeyist, false)
                && getDistanceTo(hockeyist) < game.getStickLength()
                && fabs(getAngleTo(hockeyist)) < game.getStickSector()/2.0)
        {
            move.setAction(ActionType::STRIKE);
            return true;
        }
    }
    if (fabs(getAngleTo(puck)) < game.getStickSector()/2.0
            && getDistanceTo(puck) < game.getStickLength())
    {
        if (isStrikeGetGoal(*this, world, game, false, 0, 0, true) >= CHANCE_PUCK_STRIKE_GOALIE)
        {
            return false;
        }
        if (isStrikeGetGoal(*this, world, game, false) > MIN_STRIKE_PROBABILILITY)
        {
            move.setAction(ActionType::STRIKE);
            return true;
        }
        if (puck.getOwnerPlayerId() != -1)
        {
            move.setAction(ActionType::STRIKE);
        }
        else
        {
            if ((fabs(world.getPuck().getSpeedY()) >= game.getGoalieMaxSpeed()
                    && world.getPuck().getDistanceTo(world.getOpponentPlayer().getNetFront(), MIDDLE_BY_Y) < MAX_DISTANCE_TO_SPEED_UP)
                    )
            {
                if (fabs(getAngleTo(world.getOpponentPlayer().getNetFront(), MIDDLE_BY_Y)) < MAX_ANGLE_TO_TAKE_PASS)
                {
                    move.setAction(ActionType::STRIKE);
                    return true;
                }
                return false;
            }
            else
            {
                move.setAction(ActionType::TAKE_PUCK);
            }
        }
        return true;
    }
    return false;
}

void MyAgressiveHockeyist::moveToPuck(const World& world, const Game& game, model::Move &move)
{
    const Puck& puck = world.getPuck();
//    print_debug(cout, world.getTick(), puck.getX());
    // want to take control
    if (takePuckToDefense(world, game, move))
    {
        return;
    }
    double minDistToPuck = 100500;
    for (auto hockeyist : world.getHockeyists())
    {
        if (isActiveHockeyist(hockeyist, true)
                && hockeyist.getDistanceTo(world.getPuck()) < minDistToPuck)
        {
            minDistToPuck = hockeyist.getDistanceTo(world.getPuck());
        }
    }
    if (getDistanceTo(world.getPuck()) == minDistToPuck)
    {
        Point puckPlace = getPuckBestPlace(*this, world, game, 0, 0, 0, 0);
        if (world.getPuck().getOwnerPlayerId() == world.getOpponentPlayer().getId())
        {
            puckPlace = world.getPuck();
        }
        moveTo(*this, world, game, move, puckPlace, 0);
        return;
    }
    if (world.getPuck().getOwnerPlayerId() == world.getOpponentPlayer().getId())
    {
        const Hockeyist* target = getPuckOwner(world);
        int countNearestHockeyists = 0;
        for (auto hockeyist : world.getHockeyists())
        {
            if (isActiveHockeyist(hockeyist, true)
                    && target->getDistanceTo(hockeyist) < target->getDistanceTo(*this))
            {
                countNearestHockeyists++;
            }

        }
        if (countNearestHockeyists > 1)
        {
            Point aim = getSafeFlank(*this, world);
            moveTo(*this, world, game, move, aim, getFarOpponentCorner(*this, world), true);
        }
        else
        {
            moveTo(*this, world, game, move, Point(target->getX(), target->getY()), 0);
        }
        return;
    }
    Point aim = *this;
    for (auto hockeyist : world.getHockeyists())
    {
        if (hockeyist.getId() != puck.getOwnerHockeyistId()
                && !hockeyist.isTeammate()
                && hockeyist.getType() != HockeyistType::GOALIE
                && (hockeyist.getDistanceTo(puck) < aim.getDistanceTo(puck)
                    || aim.getId() == getId())
        )
        {
            aim = hockeyist;
        }
    }
    if (aim.getId() != getId())
    {
        moveTo(*this, world, game, move, aim, 0);
        return;
    }

    const vector<Hockeyist>& hockeyists = world.getHockeyists();
    aim = getPuckBestPlace(*this, world, game, 0, 0, 0, 0);
    double distanceToGoalnet = aim.getX() - world.getMyPlayer().getNetFront();
    if (fabs(puck.getX() - world.getMyPlayer().getNetBack()) < MIDDLE_BY_X
            || (fabs(distanceToGoalnet) < MIDDLE_BY_X
            && distanceToGoalnet * puck.getSpeedX() < 0)
            )
    {
        if (fabs(puck.getX() - world.getMyPlayer().getNetBack()) < MIDDLE_BY_X)
        {
            moveTo(*this, world, game, move, puck, 0);
        }
        else
        {
            moveTo(*this, world, game, move, aim, 0);
        }
        return;
    }
    for (auto hockeyist : hockeyists)
    {
        if (hockeyist.getType() != HockeyistType::GOALIE &&
                hockeyist.isTeammate() && hockeyist.getId() != getId()
                && hockeyist.getDistanceTo(getGoalnetDefensePlace(world)) < getDistanceTo(getGoalnetDefensePlace(world)))
        {
            if (puck.getOwnerHockeyistId() == -1)
            {
                moveTo(*this, world, game, move, aim, 0);
            }
            else
            {
                const Hockeyist* target = getPuckOwner(world);
                moveTo(*this, world, game, move,
                        Point(target->getX() + BINDING_PUCK_DISTANCE * cos(target->getAngle())
                                        + target->getSpeedX(),
                                target->getY() + BINDING_PUCK_DISTANCE * sin(target->getAngle())
                                        + target->getSpeedY()), puck, false);
            }
            return;
        }
    }
    if (world.getPuck().getOwnerPlayerId() == world.getOpponentPlayer().getId())
    {
        defenseGoalNet(world, game, move);
    }
    else
    {
        Point aim2(puck);
        for (auto hockeyist : hockeyists)
        {
            if (hockeyist.getType() != HockeyistType::GOALIE &&
                    !hockeyist.isTeammate() && hockeyist.getId() != getId()
                    && getDistanceTo(hockeyist) < getDistanceTo(aim2))
            {
                aim2 = hockeyist;
            }
        }
        moveTo(*this, world, game, move, aim2, 0);
        takePuckToDefense(world, game, move);
    }
}

void MyAgressiveHockeyist::defenseGoalNet(const World& world, const Game& game, model::Move &move)
{
    if (world.getPuck().getOwnerPlayerId() == world.getMyPlayer().getId())
    {
        moveToGoalNet(world, game, move);
    }
    else
    {
        MyStandartHockeyist self2{*this};
        self2.defenseGoalNet(world, game, move);
    }
}

void MyAgressiveHockeyist::strikeToGoalNet(const model::World &world, const model::Game &game, model::Move &move) const
{
    MyStandartHockeyist standartSelf{*this};
    standartSelf.strikeToGoalNet(world, game, move);
}

void MyAgressiveHockeyist::moveToGoalNet(const model::World &world, const model::Game &game, model::Move &move)
{
    bool puckOwner = getId() == world.getPuck().getOwnerHockeyistId();

//    Point aim = getNearestOpponentCorner(*this, world, puckOwner);

    if (puckOwner)
    {
        MyStandartHockeyist self2{*this};
        self2.moveToGoalNet(world, game, move);
    }
    else
    {
        if (takePuckToDefense(world, game, move))
        {
            return;
        }
        Point aim = getAttackPlace(*this, world, game);
        long long defenseManId = Singleton::getInstance().getIdMinDefenseNoPuck();
        if (fabs(getX() - world.getMyPlayer().getNetBack()) < MIDDLE_BY_X)
        {
            for (auto hockeyist : world.getHockeyists())
            {
                if (hockeyist.getId() == defenseManId)
                {
                    if (hockeyist.getStamina() < getStamina()
                            && hockeyist.getStamina() < STAMINA_CHANGE_COEF_IN_GAME * STAMINA_MAX_HOCKEYIST)
                    {
                        moveTo(*this, world, game, move, getGoalnetDefensePlace(world), world.getPuck(), true);
                        return;
                    }
                }
            }
        }
        if (world.getPuck().getOwnerPlayerId() == world.getMyPlayer().getId())
        {
            double minTimeToEnemy = INF;
            Point aimHockeyist;
            const Hockeyist* puckOwnerH = getPuckOwner(world);
            for (auto hockeyist : world.getHockeyists())
            {
                if (isActiveHockeyist(hockeyist, false))
                {
                    if (fabs(hockeyist.getX() - world.getOpponentPlayer().getNetFront())
                            < fabs(puckOwnerH->getX() - world.getOpponentPlayer().getNetFront())
                            && timeTo(*this, game, hockeyist, false) < minTimeToEnemy)
                    {
                        minTimeToEnemy = timeTo(*this, game, hockeyist, false);
                        aimHockeyist = hockeyist;
                    }
                }
            }
            if (fabs(getX() - world.getOpponentPlayer().getNetFront()) < fabs(puckOwnerH->getX() - world.getOpponentPlayer().getNetFront())
                    && minTimeToEnemy != INF)
            {
                moveTo(*this, world, game, move, aimHockeyist, 0);
                return;
            }
        }
        if (world.getPuck().getOwnerPlayerId() == world.getOpponentPlayer().getId()
                && fabs(world.getPuck().getX()  - world.getMyPlayer().getNetFront()) < MIDDLE_BY_X)
        {
            moveTo(*this, world, game, move, aim, Point(getGoalie(world, true)->getX(), getGoalie(world, true)->getY()), true);
        }
        else
        {
            moveTo(*this, world, game, move, aim, world.getPuck(), true);
        }
    }
}

bool MyAgressiveHockeyist::solveTroubles(const model::World &world, const model::Game &game, model::Move &move)
{
    MyStandartHockeyist self2{*this};
    return self2.solveTroubles(world, game, move);
}
