#ifndef _SINGLETON_H_
#define _SINGLETON_H_

#include "MyStrategyConst.h"
#include "Point.h"
#include "MyHockeyist.h"
#include <utility>
#include <map>
#include "Strategy.h"

class Singleton {
private:
    struct MyTicks
    {
        double timeToPuck;
        double timeToDefense;
        double timeToAttack;
        double kX;
        double kY;
        double pkX;
        double pkY;
        bool resting;
    };
    Singleton()
    {
        lastTick = -1;
        hockeyistTimes.clear();
    };
    Singleton(const Singleton&) = delete;
    Singleton operator=(const Singleton&) = delete;
    int lastTick;
    const model::World* world;
    std::map<long long, MyTicks> hockeyistTimes;

public:

    static Singleton& getInstance();
    void updateInformation(const model::World &world, const model::Game &game);
    long long getHockeyistIdMinTime();
    long long getIdMinDefenseNoPuck();
    Point puckPlaceById(long long id);
    int getTick() const { return lastTick; };

};

#endif
