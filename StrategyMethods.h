#ifndef _STRATEGY_METHODS_H_
#define _STRATEGY_METHODS_H_

#include "Point.h"
#include "MyHockeyist.h"
#include "Vec2d.h"
#include <ostream>


namespace StrategyMethods {
    double getSpeed(const model::Unit &self);
    double setCorrectAngle(double& angle);
    double getCorrectAngle(const double angle);

    int substituteIndex(const MyHockeyist &self, const model::World &world, const model::Game &game);
    Point getNearestOpponentCorner(const MyHockeyist &self, const model::World &world, bool puckOwner);
    Point getFarOpponentCorner(const MyHockeyist &self, const model::World &world);
    Point getGoalnetDefensePlace(const model::World &world);
    Point getSafeFlank(const MyHockeyist &self, const model::World &world);
    bool isInBigTroubles(const MyHockeyist &self, const model::World &world, const model::Game &game);
    Point getPuckBestPlace(const MyHockeyist &self, const model::World &world, const model::Game &game, double myPX = 0, double myPY = 0, double myPVX = 0, double myPVY = 0, bool waitForPuck = false);
    bool outOfPlace(double x, double y, double r = 0);
    void simulatePuckAndWall(double& x, double& y, double& pvX, double& pvY, double r = 0);
    void decreaseSpeed(double &v, bool isHockeyist = false);
    double timeTo(const MyHockeyist &self, const model::Game &game, Point aim, bool backMove = false);
    double timeToSimple(const MyHockeyist &self, const model::Game &game, double pX, double pY, double pvX, double pvY);
    double isStrikeGetGoal(const MyHockeyist &self, const model::World &world, const model::Game &game, bool swinging = false);
    double isStrikeGetGoal(const MyHockeyist &self, const model::World &world, const model::Game &game, bool swinging, double angleDeriviation, double angleTurn, bool justCheckPuck);
    model::Hockeyist const * getGoalie(const model::World &world, bool opponent = true);
    double angleBetween(const Vec2d& v1, const Vec2d& v2);
    double isPuckOutOfGoalnet(double &pX, double &pY, double &pvX, double &pvY, const model::World &world);
    bool isLeftSide(const model::World &world, bool isMe = false);
    double simulatePuckAndGoalnetWall(double &pX, double &pY, double &pvX, double &pvY, const double borderX, const double borderY, bool top);
    double strikeByPass(const MyHockeyist &self, const model::World& world, const model::Game &game);
    const model::Hockeyist* getPuckOwner(const model::World& world);
    double deltaGoalByStrike(const MyHockeyist &self, const model::World& world, const model::Game &game);
    bool isActiveHockeyist(const model::Hockeyist &self, bool teammate);
    Point getAttackPlace(const MyHockeyist &self, const model::World &world, const model::Game &game);

    template <typename T>
    T sqr(T x) { return x * x; };

    template <typename T>
    void print_debug(std::ostream& f, const T& var)
    {
        f << var << "\n";
    }

    template <typename T, typename... Ts>
    void print_debug(std::ostream& f, const T& var, const Ts& ... ts)
    {
        f << var << " ";
        print_debug(f, ts...);
    }

}

#endif