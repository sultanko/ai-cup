#include "Singleton.h"
#include "MyStandartHockeyist.h"
#include "StrategyMethods.h"
#include "MyStrategyConst.h"

using namespace std;
using namespace model;
using namespace StrategyMethods;
using namespace MyStrategyConst;

Singleton& Singleton::getInstance()
{
    static Singleton instance;
    return instance;
}

void Singleton::updateInformation(const World &world, const Game &game)
{
    if (world.getTick() == lastTick)
    {
        return;
    }
    lastTick = world.getTick();
    this->world = &world;
//    hockeyistTimes.clear();
    for (auto hockeyist : world.getHockeyists())
    {
        if (hockeyist.isTeammate() && hockeyist.getType() != GOALIE)
        {
            MyStandartHockeyist self2{hockeyist};
            MyTicks now;
            now.resting = (hockeyist.getState() == HockeyistState::RESTING);
            if (!now.resting)
            {
                Point aim = getPuckBestPlace(self2, world, game);
                now.timeToPuck = timeTo(self2, game, aim, false);
                now.timeToDefense = min(timeTo(self2, game, getGoalnetDefensePlace(world), false),
                                        timeTo(self2, game, getGoalnetDefensePlace(world), true));
                now.timeToAttack = timeTo(self2, game, getAttackPlace(self2, world, game), false);
                now.kX = hockeyist.getX();
                now.kY = hockeyist.getY();
                now.pkX = aim.getX();
                now.pkY = aim.getY();
//                print_debug(cout, world.getTick(), hockeyist.getId(), now.kX, now.kY, now.timeToDefense);
            }
            hockeyistTimes[hockeyist.getId()] = now;
/*            if (hockeyistTimes.find(hockeyist.getId()) != hockeyistTimes.end())
            {
            }
            else
            {
                hockeyistTimes.insert(make_pair(hockeyist.getId(), now));
            }*/
        }
    }
}

long long Singleton::getHockeyistIdMinTime()
{
    double minCountTicks = INF;
    long long hockeyistId = -1;
    double minKX = INF;
    double minKY = INF;
    for (auto it = hockeyistTimes.begin(); it != hockeyistTimes.end(); it++)
    {
        if (it->second.resting)
        {
            continue;
        }
        if (it->second.timeToPuck < minCountTicks)
        {
            minCountTicks = it->second.timeToPuck;
            hockeyistId = it->first;
            minKX = it->second.kX;
            minKY = it->second.kY;
        }
        else if (it->second.timeToPuck == minCountTicks
                && sqrt(sqr(it->second.kX - world->getPuck().getX()) + sqr(it->second.kY - world->getPuck().getY()))
                < sqrt(sqr(minKX - world->getPuck().getX()) + sqr(minKY - world->getPuck().getY())))
        {
            minCountTicks = it->second.timeToPuck;
            hockeyistId = it->first;
            minKX = it->second.kX;
            minKY = it->second.kY;
        }
    }
    return hockeyistId;
}

long long Singleton::getIdMinDefenseNoPuck()
{
    long long minId = getHockeyistIdMinTime();
    double minDist = INF;
    double minKX = INF;
    long long hockeyistId = -1;
    Point aim = getGoalnetDefensePlace(*world);
    for (auto it = hockeyistTimes.begin(); it != hockeyistTimes.end(); it++)
    {
        if (it->second.resting || it->first == minId)
        {
            continue;
        }
        double dist = sqrt(sqr(aim.getX() - it->second.kX) + sqr(aim.getY() - it->second.kY));
        if (dist < minDist)
        {
            minDist = dist;
            hockeyistId = it->first;
            minKX = it->second.kX;
        }
        else if (dist == minDist
                && fabs(it->second.kX - world->getMyPlayer().getNetFront()) < fabs(minKX - world->getMyPlayer().getNetFront()))
        {
            minDist = it->second.timeToDefense;
            hockeyistId = it->first;
            minKX = it->second.kX;
        }
    }
//    print_debug(cout, lastTick, hockeyistId);
    return hockeyistId;
}

Point Singleton::puckPlaceById(long long id)
{
    return Point(hockeyistTimes[id].pkX, hockeyistTimes[id].pkY);
}
