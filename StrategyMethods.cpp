#include <string.h>
#include "StrategyMethods.h"
#include "MyStrategyConst.h"

using namespace std;
using namespace MyStrategyConst;
using namespace model;

double StrategyMethods::getSpeed(const Unit &self)
{
    return sqrt(self.getSpeedX()*self.getSpeedX() + self.getSpeedY()*self.getSpeedY());
}

bool StrategyMethods::isInBigTroubles(const MyHockeyist &self, const model::World &world, const model::Game &game)
{
    if (world.getPuck().getOwnerHockeyistId() != self.getId())
    {
        return false;
    }
    Puck puck = world.getPuck();
    int countDangerousHockeyist = 0;
    for (auto hockeyist : world.getHockeyists())
    {
        if (isActiveHockeyist(hockeyist, false)
                && fabs(hockeyist.getAngleTo(puck)) < game.getStickSector()/2.0
                && hockeyist.getDistanceTo(puck) < game.getStickLength())
        {
            countDangerousHockeyist++;
            continue;
        }
        if (isActiveHockeyist(hockeyist, false)
                && fabs(self.getAngleTo(hockeyist)) < game.getStickSector()/2.0
                && fabs(hockeyist.getAngleTo(self)) < game.getStickSector()/2.0
                && hockeyist.getDistanceTo(self) < game.getStickLength() * 2
                && fabs(self.getX() - world.getMyPlayer().getNetBack()) < MIDDLE_BY_X)
        {
            countDangerousHockeyist++;
        }
    }
    return (countDangerousHockeyist > 1);
}

Point StrategyMethods::getGoalnetDefensePlace(const model::World &world)
{
    double x = world.getMyPlayer().getNetFront();
    double y = (world.getMyPlayer().getNetTop() + world.getMyPlayer().getNetBottom())/2.0;
    double distanceCoef = 1;
    if (world.getPuck().getOwnerPlayerId() == world.getMyPlayer().getId()
            && fabs(world.getPuck().getX() - world.getMyPlayer().getNetFront()) >
                2 * DISTANSE_FROM_MY_GOALNET_X)
    {
        distanceCoef = 1;
    }
    if (x == world.getMyPlayer().getNetLeft())
    {
        x -= distanceCoef * DISTANSE_FROM_MY_GOALNET_X;
    }
    else
    {
        x += distanceCoef * DISTANSE_FROM_MY_GOALNET_X;
    }
    return Point(x, y);
}

Point StrategyMethods::getSafeFlank(const MyHockeyist &self, const model::World &world)
{
    int countTop = 0;
    int countBottom = 0;
    for (auto hockeyist : world.getHockeyists())
    {
        if (isActiveHockeyist(hockeyist, false))
        {
            if (hockeyist.getY() <= self.getY() )
            {
                countTop++;
            }
            else
            {
                countBottom++;
            }
        }
    }
    return Point(world.getOpponentPlayer().getNetFront() < MIDDLE_BY_X ? MIDDLE_BY_X + DISTANCE_FOR_FLANK_X : MIDDLE_BY_X - DISTANCE_FOR_FLANK_X,
            countBottom < countTop || (countBottom == countTop && self.getY() > MIDDLE_BY_Y) ?
                    BORDER_BOTTOM_Y - DISTANCE_FOR_FLANK_Y : BORDER_TOP_Y + DISTANCE_FOR_FLANK_Y);
}

Point StrategyMethods::getNearestOpponentCorner(const MyHockeyist &self, const model::World &world, bool puckOwner)
{
    const Player& enemyPlayer = world.getOpponentPlayer();
    const Puck& puck = world.getPuck();
    double x = enemyPlayer.getNetFront()
            + (enemyPlayer.getNetFront() == enemyPlayer.getNetRight() ?
            DISTANCE_TO_GOALNET_X : -DISTANCE_TO_GOALNET_X);
    double y = enemyPlayer.getNetBottom() + DISTANCE_TO_GOALNET_Y;
    if (puck.getDistanceTo(x, enemyPlayer.getNetTop() - DISTANCE_TO_GOALNET_Y)
            < puck.getDistanceTo(x, y) )
    {
        y = enemyPlayer.getNetTop() - DISTANCE_TO_GOALNET_Y;
    }
    if (puck.getY() >= world.getOpponentPlayer().getNetTop() && puck.getY() <= world.getOpponentPlayer().getNetBottom())
    {
        y = enemyPlayer.getNetBottom() + DISTANCE_TO_GOALNET_Y;
        if (self.getDistanceTo(x, enemyPlayer.getNetTop() - DISTANCE_TO_GOALNET_Y)
                < self.getDistanceTo(x, y) )
        {
            y = enemyPlayer.getNetTop() - DISTANCE_TO_GOALNET_Y;
        }
    }
    if (!puckOwner)
    {
        if (puck.getDistanceTo(x, enemyPlayer.getNetTop() - DISTANCE_TO_GOALNET_Y) >
                puck.getDistanceTo(x, enemyPlayer.getNetBottom() + DISTANCE_TO_GOALNET_Y))
        {
            y = enemyPlayer.getNetTop() - DISTANCE_TO_GOALNET_Y;
        }
        else
        {
            y = enemyPlayer.getNetBottom() + DISTANCE_TO_GOALNET_Y;
        }
        if (puck.getY() >= world.getOpponentPlayer().getNetTop() && puck.getY() <= world.getOpponentPlayer().getNetBottom())
        {
            y = enemyPlayer.getNetBottom() + DISTANCE_TO_GOALNET_Y;
            if (self.getDistanceTo(x, enemyPlayer.getNetTop() - DISTANCE_TO_GOALNET_Y)
                    > self.getDistanceTo(x, y) )
            {
                y = enemyPlayer.getNetTop() - DISTANCE_TO_GOALNET_Y;
            }
        }
    }
    return Point(x, y);
//    return Point(MIDDLE_BY_X, BORDER_BOTTOM_Y);
}

Point StrategyMethods::getFarOpponentCorner(const MyHockeyist &self, const model::World &world)
{
    double x = world.getOpponentPlayer().getNetFront();
    if (x == world.getOpponentPlayer().getNetLeft())
    {
        x = (fabs(self.getX() - x) > MIDDLE_BY_X ? world.getOpponentPlayer().getNetBack() : x + PUCK_RADIUS);
    }
    else
    {
        x = (fabs(self.getX() - x) > MIDDLE_BY_X ? world.getOpponentPlayer().getNetBack() : x - PUCK_RADIUS);
    }
    double y = world.getOpponentPlayer().getNetBottom();
    if (world.getPuck().getDistanceTo(x, y) < world.getPuck().getDistanceTo(x, world.getOpponentPlayer().getNetTop()))
    {
        y = world.getOpponentPlayer().getNetTop();
    }
    return Point(x, y);
}

void StrategyMethods::decreaseSpeed(double &v, bool isHockeyist)
{
    if (!isHockeyist)
    {
        v *= BRAKE_DOWN_COEF;
    }
    else
    {
        v *= BRAKE_DOWN_COEF_HOCKEY;
    }
}

bool StrategyMethods::outOfPlace(double x, double y, double r)
{
    return (x < BORDER_LEFT_X + r || x > BORDER_RIGHT_X - r
            || y < BORDER_TOP_Y + r || y > BORDER_BOTTOM_Y - r);
//            || (y >= BORDER_GOALNET_TOP_Y && y <=BORDER_GOALNET_BOTTOM_Y
//                && (x < BORDER_LEFT_X + r + 2 *HOCKEYIST_RADIUS
//                        || x > BORDER_RIGHT_X - r - 2 * HOCKEYIST_RADIUS)

}

void StrategyMethods::simulatePuckAndWall(double &x, double &y, double &pvX, double &pvY, double r)
{
    if (x + r > BORDER_RIGHT_X || x - r < BORDER_LEFT_X )
    {
        double penetration = (x + r > BORDER_RIGHT_X ? x + r - BORDER_RIGHT_X : BORDER_LEFT_X - x + r);
        if (penetration/PUCK_RADIUS < ENABLED_DEFORMATION)
        {
            return;
        }
        pvX = pvX * WALL_BRAKE_COEF;
        if (pvX < penetration)
        {
            x = (x + r > BORDER_RIGHT_X ? BORDER_RIGHT_X - penetration*PENETRATION_COEF - r :
                    BORDER_LEFT_X + penetration*PENETRATION_COEF + r);
        }
    }
    else if (y + r > BORDER_BOTTOM_Y || y - r < BORDER_TOP_Y)
    {
        double penetration = (y + r > BORDER_BOTTOM_Y ? x + r - BORDER_BOTTOM_Y : BORDER_TOP_Y - x + r);
        if (penetration/PUCK_RADIUS < ENABLED_DEFORMATION)
        {
            return;
        }
        pvY *= WALL_BRAKE_COEF;
        if (pvY < penetration)
        {
            y = (y + r > BORDER_BOTTOM_Y ? BORDER_BOTTOM_Y - penetration*PENETRATION_COEF - r :
                    BORDER_TOP_Y + penetration*PENETRATION_COEF + r);
        }
    }
/*    else if (y >= BORDER_GOALNET_TOP_Y && y <= BORDER_GOALNET_BOTTOM_Y)
    {
        double penetration = (x + r + 2 *HOCKEYIST_RADIUS > BORDER_RIGHT_X ? x + r + 2 * HOCKEYIST_RADIUS - BORDER_RIGHT_X
                : BORDER_LEFT_X - x + r + 2 * HOCKEYIST_RADIUS);
        if (penetration/PUCK_RADIUS < ENABLED_DEFORMATION)
        {
            return;
        }
        pvX = pvX * WALL_BRAKE_COEF;
        if (pvX < penetration)
        {
            x = (x + r + 2 * HOCKEYIST_RADIUS > BORDER_RIGHT_X ? BORDER_RIGHT_X - penetration*PENETRATION_COEF - r - 2 * HOCKEYIST_RADIUS:
                    BORDER_LEFT_X + penetration*PENETRATION_COEF + r + 2 * HOCKEYIST_RADIUS);
        }
    }*/
}

double StrategyMethods::timeTo(const MyHockeyist &self, const model::Game &game, Point aim, bool backMove)
{
    int result = 0;
    if (self.getDistanceTo(aim) < HOCKEYIST_RADIUS/2.0)
    {
        return 1;
    }
    if (self.getDistanceTo(aim)/game.getHockeyistMaxSpeed() > TICKS_FOR_PUCK)
    {
        return TICKS_FOR_PUCK + 1;
    }
    double sX = self.getX();
    double sY = self.getY();
    double svX = self.getSpeedX();
    double svY = self.getSpeedY();
    double selfAngle = self.getAngle();
    while (true)
    {
        Point nowSelf(self.getId(), self.getMass(), self.getRadius(), sX, sY, svX, svY, selfAngle, self.getAngularSpeed());
        Vec2d opt_dir = (Vec2d(aim.getX() - sX, aim.getY() - sY))
//            *game.getHockeyistMaxSpeed()
                - (Vec2d(svX, svY))
        ;
        double angleToUnit = nowSelf.getAngleTo(sX + opt_dir.getX(), sY + opt_dir.getY());
//        double angleToUnit = nowSelf.getAngleTo(aim);
        if (sqrt(sqr(sX - aim.getX()) + sqr(sY - aim.getY())) < game.getStickLength()
                && fabs(nowSelf.getAngleTo(aim)) < game.getStickSector()/2.0
                && (self.getRemainingCooldownTicks() - result <= 0)
                && (self.getRemainingKnockdownTicks() - result <= 0)
                )
        {
            break;
        }
        if (result > TICKS_FOR_PUCK)
        {
            break;
        }
        if (backMove)
        {
            angleToUnit -= M_PI;
            setCorrectAngle(angleToUnit);
        }
        selfAngle += (angleToUnit < 0 ? -1 : 1) * min(fabs(angleToUnit), game.getHockeyistTurnAngleFactor() * self.getHockeyistTurnDereviation());
        setCorrectAngle(selfAngle);
        double speedCoef = ((backMove != (fabs(angleToUnit) >= MAX_ANGLE_TO_SPEED_UP)) ? SPEED_DOWN_HOCKEYIST_FACTOR : SPEED_UP_HOCKEYIST_FACTOR) * self.getHockeyistSpeedDereviation();
        svX += cos(selfAngle) * speedCoef;
        svY += sin(selfAngle) * speedCoef;
        decreaseSpeed(svX, true);
        decreaseSpeed(svY, true);
        sX += svX;
        sY += svY;
        result++;
    }
//    print_debug(cout, "TimeTo: ", result);
    return result;
}

Point StrategyMethods::getPuckBestPlace(const MyHockeyist &self, const model::World &world, const model::Game &game,
        double myPX, double myPY, double myPVX, double myPVY, bool waitForPuck)
{
    const Puck& puck = world.getPuck();
//    double sX = getX();
//    double sY = getY();
    double pX = puck.getX();
    double pY = puck.getY();
    double pvX = puck.getSpeedX();
    double pvY = puck.getSpeedY();
    if (puck.getOwnerHockeyistId() != -1)
    {
//        const Hockeyist *puckOwner = getPuckOwner(world);
//        pvX = puckOwner->getSpeedX();
//        pvY = puckOwner->getSpeedY();
    }
    if (myPX != 0)
    {
        pX = myPX;
        pY = myPY;
        pvX = myPVX;
        pvY = myPVY;
    }
    int countTicks = 0;
    while (countTicks < TICKS_FOR_PUCK)
    {
//        print_debug(cout, "PuckTicks: ", countTicks);
        if (self.getDistanceTo(pX, pY)/game.getHockeyistMaxSpeed()
                + (fabs(self.getAngleTo(pX, pY) - game.getStickSector())/(game.getHockeyistTurnAngleFactor() * self.getHockeyistTurnDereviation())
                < countTicks))
        {
            if (!waitForPuck)
            {
                double nowTimeTo = timeTo(self, game, Point(pX, pY), false);
                if (nowTimeTo < countTicks)
                {
                    return Point(pX, pY);
                }
            }
        }
        if (waitForPuck && self.getDistanceTo(pX, pY) < game.getStickLength()
                && fabs(self.getAngleTo(pX, pY)) < game.getStickSector()/2.0)
        {
            return Point(pX, pY);
        }
        if (waitForPuck && countTicks > TICKS_FOR_PUCK/2.0)
        {
            return Point(pX, pY);
        }
        if (outOfPlace(pX, pY, puck.getRadius()))
        {
            simulatePuckAndWall(pX, pY, pvX, pvY, puck.getRadius());
        }
        else
        {
            pX += pvX;
            pY += pvY;
        }
        if (sqrt(sqr(pX) + sqr(pY)) < MIN_PUCK_SPEED_FOR_BRUTEFORCE)
        {
            return Point(pX, pY);
        }
        decreaseSpeed(pvX, false);
        decreaseSpeed(pvY, false);
        countTicks++;
    }
    return Point(pX, pY);
}

double StrategyMethods::simulatePuckAndGoalnetWall(double &pX, double &pY, double &pvX, double &pvY, const double borderX, const double borderY, bool top)
{
/*    if (1 - sqrt(sqr(borderX - pX) + sqr(borderY - pY))/PUCK_RADIUS < ENABLED_DEFORMATION)
    {
        return 0;
    }*/
/*    double oldpX = pX - pvX;
    double oldpY = pY - pvY;

    double kWall = (borderY - (top ? BORDER_TOP_Y : BORDER_BOTTOM_Y))/
            (borderX - (borderX > MIDDLE_BY_X ? BORDER_RIGHT_BACK_X : BORDER_LEFT_BACK_X));

    double kPuck = (oldpY - (top ? BORDER_TOP_Y : BORDER_BOTTOM_Y))/
            (oldpX - (borderX > MIDDLE_BY_X ? BORDER_RIGHT_BACK_X : BORDER_LEFT_BACK_X));*/


/*    if ((top && kPuck <= kWall && borderX < MIDDLE_BY_X)
            || ((!top) && (kPuck >= kWall) && (borderX < MIDDLE_BY_X))
            || (top && kPuck >= kWall && borderX > MIDDLE_BY_X)
            || (!top && kPuck <= kWall && borderX > MIDDLE_BY_X))
    {
        return 1.0;
    }*/
    float penetration = (float) fabs(borderY - pY - PUCK_RADIUS);
    pY = borderY + (top ? 1 : -1) * (penetration * PENETRATION_COEF + PUCK_RADIUS);

    pvY *= WALL_GOALNET_BRAKE_COEF;
    return 0;
}

double StrategyMethods::isPuckOutOfGoalnet(double &pX, double &pY, double &pvX, double &pvY, const model::World &world)
{
    if (outOfPlace(pX, pY, PUCK_RADIUS))
    {
        if (pY <= world.getOpponentPlayer().getNetTop()
                || pY >= world.getOpponentPlayer().getNetBottom())
        {
            return 1.0;
        }
        if (pY - PUCK_RADIUS > world.getOpponentPlayer().getNetTop()
                && pY + PUCK_RADIUS < world.getOpponentPlayer().getNetBottom())
        {
            return 0;
        }
        if (pY + PUCK_RADIUS >= world.getOpponentPlayer().getNetBottom())
        {
            return simulatePuckAndGoalnetWall(pX, pY, pvX, pvY, world.getOpponentPlayer().getNetFront(), world.getOpponentPlayer().getNetBottom(), false);
        }
        else if (pY - PUCK_RADIUS <= world.getOpponentPlayer().getNetTop())
        {
            return simulatePuckAndGoalnetWall(pX, pY, pvX, pvY, world.getOpponentPlayer().getNetFront(), world.getOpponentPlayer().getNetTop(), true);
        }
        return 1.0;
    }
    return 0;
}

bool strikeDontGetGoal(double pX, double pY, double pvX, double pvY, double borderY)
{
    if (fabs(pvY) < GOALIE_MAX_SPEED) { return true; }
    if ((borderY - pY) * pvY < 0) { return true; }
    double intersectX = pX + pvX * (borderY - pY)/pvY;
    if (intersectX >= BORDER_LEFT_X - 5 && intersectX <= BORDER_RIGHT_X + 5 ) { return true; }
    if (intersectX < BORDER_LEFT_BACK_X - MAX_PUCK_BRUTFORCE_X) { return true; }
    if (intersectX > BORDER_RIGHT_BACK_X + MAX_PUCK_BRUTFORCE_X) { return true; }
    return false;
}

double StrategyMethods::isStrikeGetGoal(const MyHockeyist &self, const model::World &world, const model::Game &game, bool swinging, double angleDeriviation, double angleTurn, bool justCheckPuck)
{
    double sX = self.getX();
    double sY = self.getY();
    double svX = self.getSpeedX();
    double svY = self.getSpeedY();
    double selfAngle = self.getAngle();
    double angularSpeed = self.getAngularSpeed();
    if (fabs(angularSpeed) > M_PI/4.0)
    {
        angularSpeed = 0;
    }
    if (angleTurn != 0)
    {
        int ticksForTurn = (int) ceil(fabs(angleTurn) /
                (game.getHockeyistTurnAngleFactor() * self.getHockeyistTurnDereviation()));
        while (ticksForTurn > 0)
        {
            sX += svX;
            sY += svY;
            selfAngle += angularSpeed;
            angularSpeed -= ANGULAR_SPEED_BRAKE_DOWN_COEF * angularSpeed;
            decreaseSpeed(svX, true);
            decreaseSpeed(svY, true);
            ticksForTurn--;
        }
        selfAngle += angleTurn;
    }
    if (swinging)
    {
        int counter = game.getMaxEffectiveSwingTicks() + 1;
        while (counter > 0)
        {
            selfAngle += angularSpeed;
            angularSpeed -= ANGULAR_SPEED_BRAKE_DOWN_COEF * angularSpeed;
            sX += svX;
            sY += svY;
            decreaseSpeed(svX, true);
            decreaseSpeed(svY, true);
            counter--;
        }
    }
    double pX = sX + cos(selfAngle) * game.getPuckBindingRange();
    double pY = sY + sin(selfAngle) * game.getPuckBindingRange();
    if (world.getPuck().getOwnerHockeyistId() != self.getId())
    {
        if (self.getDistanceTo(world.getPuck()) < game.getStickLength()
                && fabs(self.getAngleTo(world.getPuck())) < game.getStickSector()/2.0)
        {
            pX = world.getPuck().getX();
            pY = world.getPuck().getY();
        }
    }
    setCorrectAngle(selfAngle);
    double strikePowerCoef = (swinging ? game.getStrikePowerGrowthFactor() * game.getMaxEffectiveSwingTicks() : 0);
    if (self.getState() == HockeyistState::SWINGING)
    {
        strikePowerCoef = game.getStrikePowerGrowthFactor() * min(self.getSwingTicks(), game.getMaxEffectiveSwingTicks());
    }
    Point nowSelf(self.getId(), self.getMass(), self.getRadius(), sX, sY, svX, svY, selfAngle, self.getAngularSpeed());
    double puckSpeed = self.getPuckSpeedDereviation() * game.getStruckPuckInitialSpeedFactor() *
            (strikePowerCoef + game.getPassPowerFactor())
            + sqrt(svX * svX + svY * svY)
            * cos(-nowSelf.getAngleTo(sX + svX, sY + svY) + (angleDeriviation));
    double pvX = puckSpeed * cos(selfAngle + angleDeriviation);
    double pvY = puckSpeed * sin(selfAngle + angleDeriviation);
    if (justCheckPuck)
    {
        pX = world.getPuck().getX();
        pY = world.getPuck().getY();
        pvX = world.getPuck().getSpeedX();
        pvY = world.getPuck().getSpeedY();
    }
    if (
//            (pY < world.getOpponentPlayer().getNetTop() || pY > world.getOpponentPlayer().getNetBottom()) &&
            strikeDontGetGoal(pX, pY, pvX, pvY, (pY < MIDDLE_BY_Y ? world.getOpponentPlayer().getNetBottom() : world.getOpponentPlayer().getNetTop())))
    {
        return 0;
    }
    double gX = 0;
    double gY = 0;
    if (getGoalie(world, true) != nullptr)
    {
        gX = getGoalie(world, true)->getX();
        gY = getGoalie(world, true)->getY();
    }
    if (pY < world.getOpponentPlayer().getNetTop())
    {
        gY = world.getOpponentPlayer().getNetTop() + HOCKEYIST_RADIUS;
    }
    else if (pY > world.getOpponentPlayer().getNetBottom())
    {
        gY = world.getOpponentPlayer().getNetBottom() - HOCKEYIST_RADIUS;
    }
    else if (angleTurn != 0)
    {
        gY = pY;
    }
    int countTicks = 0;
    while (countTicks < TICKS_FOR_GOAL_PUCK)
    {
        if (sqrt(sqr(pX-gX) + sqr(pY-gY)) <= (PUCK_RADIUS + HOCKEYIST_RADIUS))
        {
            if (1 - sqrt(sqr(pX - gX) + sqr(pY - gY))/(PUCK_RADIUS + HOCKEYIST_RADIUS) > ENABLED_DEFORMATION)
            {
                if (fabs(pX - world.getOpponentPlayer().getNetFront()) < fabs(gX - world.getOpponentPlayer().getNetFront()))
                {
                    return CHANCE_PUCK_STRIKE_GOALIE;
                }
                return 0;
            }
        }
        if (isPuckOutOfGoalnet(pX, pY, pvX, pvY, world) != 0)
        {
            return CHANCE_PUCK_STRIKE_WALL;
        }
        if ((!isLeftSide(world, false) && pX > world.getOpponentPlayer().getNetFront())
                || (isLeftSide(world, false) && pX < world.getOpponentPlayer().getNetFront()))
        {
            return 1;
        }
        gY += (pY < gY ? -1 : 1) * min(fabs(pY - gY), game.getGoalieMaxSpeed());
        gY = max(gY, world.getOpponentPlayer().getNetTop() + HOCKEYIST_RADIUS);
        gY = min(gY, world.getOpponentPlayer().getNetBottom() - HOCKEYIST_RADIUS);
        pX += pvX;
        pY += pvY;
        decreaseSpeed(pvX, false);
        decreaseSpeed(pvY, false);
        countTicks++;
    }
    return 0;
}

double StrategyMethods::isStrikeGetGoal(const MyHockeyist &self, const model::World &world, const model::Game &game, bool swinging)
{
    double result = 0;
    double countAngles = 0;
    for (double delta = 0; delta < game.getStrikeAngleDeviation()* self.getPuckAngleDereviation(); delta += STRIKE_DERIVIATION_DELTA)
    {
        result += isStrikeGetGoal(self, world, game, swinging, delta, 0, false);
        countAngles += 1;
        if (delta != 0)
        {
            countAngles += 1;
            result += isStrikeGetGoal(self, world, game, swinging, -delta, 0, false);
        }
        else if (result == 0)
        {
            break;
        }
    }
    if (result != 0)
    {
        result += isStrikeGetGoal(self, world, game, swinging, game.getStrikeAngleDeviation() * self.getPuckAngleDereviation(), 0, false)
                + isStrikeGetGoal(self, world, game, swinging, -game.getStrikeAngleDeviation() * self.getPuckAngleDereviation(), 0, false);
        countAngles += 2.0;
    }
    return result/countAngles;
//    return isStrikeGetGoal(self, world, game, swinging, 0);
}

bool StrategyMethods::isLeftSide(const model::World &world, bool isMe)
{
    if (isMe)
    {
        return (world.getMyPlayer().getNetFront() == world.getMyPlayer().getNetRight());
    }
    else
    {
        return (world.getOpponentPlayer().getNetFront() == world.getOpponentPlayer().getNetRight());
    }

}

Hockeyist const * StrategyMethods::getGoalie(const model::World &world, bool opponent)
{
    for (size_t i = 0; i != world.getHockeyists().size(); i++)
    {
        if (world.getHockeyists()[i].getType() == HockeyistType::GOALIE)
        {
            if ((opponent && world.getHockeyists()[i].getPlayerId() == world.getOpponentPlayer().getId())
                    || (!opponent && world.getHockeyists()[i].getPlayerId() == world.getMyPlayer().getId()))
            {
                return &world.getHockeyists()[i];
            }
        }
    }
    return nullptr;
}

double StrategyMethods::angleBetween(const Vec2d &v1, const Vec2d &v2)
{
    const Point tmp(0, 0);
    double angleV1 = tmp.getAngleTo(v1.getX(), v1.getY());
    double angleV2 = tmp.getAngleTo(v2.getX(), v2.getY());
    double angle = angleV2 - angleV1;
    while (angle > M_PI)
    {
        angle -= 2* M_PI;
    }
    while (angle < -M_PI)
    {
        angle += 2* M_PI;
    }
    return angle;
}

double ::StrategyMethods::strikeByPass(const MyHockeyist &self, const model::World &world, const model::Game &game)
{
    for (double deltaAngle = 0; deltaAngle < game.getPassSector()/2.0; deltaAngle += DELTA_FOR_ANGLE_VARIANTS)
    {
        double result = 0;
        int countAngles = 0;
        for (double delta = 0; delta < game.getPassAngleDeviation()* self.getPuckAngleDereviation(); delta += STRIKE_DERIVIATION_DELTA)
        {
            result += isStrikeGetGoal(self, world, game, false, deltaAngle + delta, 0, false);
            countAngles++;
            if (delta != 0)
            {
                countAngles++;
                result += isStrikeGetGoal(self, world, game, false, deltaAngle - delta, 0, false);
            }
            else if (result == 0)
            {
                break;
            }
        }
        if (result != 0)
        {
            result += isStrikeGetGoal(self, world, game, false, deltaAngle + game.getPassAngleDeviation() * self.getPuckAngleDereviation(), 0, false)
                    + isStrikeGetGoal(self, world, game, false, deltaAngle - game.getPassAngleDeviation() * self.getPuckAngleDereviation(), 0, false);
            countAngles += 2;
        }
        if (result/countAngles > MIN_PASS_PROBABILITY)
        {
            if (world.getTick() > 4570 && world.getTick() < 4600)
            {
//                print_debug(cout, world.getTick(), "PASS: ", deltaAngle, result/countAngles);
            }
            return deltaAngle;
        }
        result = 0;
        countAngles = 0;
        for (double delta = 0; delta < game.getPassAngleDeviation()* self.getPuckAngleDereviation(); delta += STRIKE_DERIVIATION_DELTA)
        {
            result += isStrikeGetGoal(self, world, game, false, -deltaAngle + delta, 0, false);
            countAngles++;
            if (delta != 0)
            {
                countAngles++;
                result += isStrikeGetGoal(self, world, game, false, -deltaAngle - delta, 0, false);
            }
            else if (result == 0)
            {
                break;
            }
        }
        if (result != 0)
        {
            result += isStrikeGetGoal(self, world, game, false, -deltaAngle + game.getPassAngleDeviation() * self.getPuckAngleDereviation(), 0, false)
                    + isStrikeGetGoal(self, world, game, false, -deltaAngle - game.getPassAngleDeviation() * self.getPuckAngleDereviation(), 0, false);
            countAngles += 2;
        }
        if (result/countAngles > MIN_PASS_PROBABILITY)
        {
            if (world.getTick() > 4570 && world.getTick() < 4600)
            {
//                print_debug(cout, world.getTick(), "PASS: ", -deltaAngle, result/countAngles);
            }
            return -deltaAngle;
        }
    }
    return game.getPassSector() + 1;
}

const Hockeyist *::StrategyMethods::getPuckOwner(const model::World &world)
{
    for (auto it = world.getHockeyists().begin(); it != world.getHockeyists().end(); it++)
    {
        if (it->getId() == world.getPuck().getOwnerHockeyistId())
        {
            return &(*it);
        }
    }
    return nullptr;
}

double ::StrategyMethods::deltaGoalByStrike(const MyHockeyist &self, const model::World &world, const model::Game &game)
{
    double tmp = isStrikeGetGoal(self, world, game, true);
    if (tmp > MIN_STRIKE_PROBABILILITY)
    {
        return 0;
    }
    double probability[(int)(180.0 * DELTA_FOR_STRIKE/M_PI) * 4];
    memset(probability, 0, sizeof probability);
    for (double turnAngle = M_PI/180; turnAngle < DELTA_FOR_STRIKE; turnAngle += DELTA_FOR_ANGLE_VARIANTS)
    {
        double result = 0;
        int countAngles = 0;
        for (double deltaForDelta = 0; deltaForDelta < game.getStrikeAngleDeviation()* self.getPuckAngleDereviation(); deltaForDelta += STRIKE_DERIVIATION_DELTA)
        {
            countAngles++;
            result += isStrikeGetGoal(self, world, game, true, deltaForDelta, turnAngle, false);
            if (deltaForDelta != 0)
            {
                countAngles++;
                result += isStrikeGetGoal(self, world, game, true, -deltaForDelta, turnAngle, false);
            }
            else if (result == 0)
            {
                break;
            }
        }
        if (result != 0)
        {
            result += isStrikeGetGoal(self, world, game, true, game.getStrikeAngleDeviation() * self.getPuckAngleDereviation(), turnAngle, false)
                    + isStrikeGetGoal(self, world, game, true, -game.getStrikeAngleDeviation() * self.getPuckAngleDereviation(), turnAngle, false);
            countAngles += 2;
        }
        probability[(int)(180 * (turnAngle + DELTA_FOR_STRIKE)/ M_PI)] = result/countAngles;
        countAngles = 0;
        result = 0;
        for (double deltaForDelta = 0; deltaForDelta < game.getStrikeAngleDeviation()* self.getPuckAngleDereviation(); deltaForDelta += STRIKE_DERIVIATION_DELTA)
        {
            countAngles++;
            result += isStrikeGetGoal(self, world, game, true, deltaForDelta, -turnAngle, false);
            if (deltaForDelta != 0)
            {
                countAngles++;
                result += isStrikeGetGoal(self, world, game, true, -deltaForDelta, -turnAngle, false);
            }
            else if (result == 0)
            {
                break;
            }
        }
        if (result != 0)
        {
            result += isStrikeGetGoal(self, world, game, true, game.getStrikeAngleDeviation() * self.getPuckAngleDereviation(), -turnAngle, false)
                    + isStrikeGetGoal(self, world, game, true, -game.getStrikeAngleDeviation() * self.getPuckAngleDereviation(), -turnAngle, false);
            countAngles += 2;
        }
        probability[(int)(180 * (-turnAngle + DELTA_FOR_STRIKE)/ M_PI)] = result/countAngles;
    }
    double bestAngle = DELTA_FOR_STRIKE + 1;
    double bestScore = 0;
    for (double turnAngle = -DELTA_FOR_STRIKE; turnAngle < DELTA_FOR_STRIKE; turnAngle += DELTA_FOR_ANGLE_VARIANTS)
    {
        int num;
        num = (int) (180.0 * (turnAngle + DELTA_FOR_STRIKE)/ M_PI);
        if (probability[num] > MIN_STRIKE_PROBABILILITY)
        {
            double  score = probability[num];
            for (int i = 0; i < 6; i++)
            {
                if (num + i < 360 && probability[num + i] > MIN_SWINGING_STRIKE_PROBABILITY)
                {
                    score += probability[num + i];
                }
                if (num - i >= 0 && probability[num - i] > MIN_SWINGING_STRIKE_PROBABILITY)
                {
                    score += probability[num - i];
                }
            }
            if (score > bestScore)
            {
                bestAngle = turnAngle;
                bestScore = score;
            }
        }
    }
    if (bestAngle != DELTA_FOR_STRIKE + 1)
    {
//        print_debug(cout, world.getTick(), "Angle: ", bestAngle, "Score: ", bestScore);
    }
    return bestAngle;
}

bool StrategyMethods::isActiveHockeyist(const model::Hockeyist &self, bool teammate)
{
    return (self.getType() != HockeyistType::GOALIE && self.getState() != HockeyistState::RESTING
            && self.isTeammate() == teammate);
}

Point StrategyMethods::getAttackPlace(const MyHockeyist &self, const model::World &world, const model::Game &game)
{
/*    const Hockeyist* puckOwner = getPuckOwner(world);
    if (puckOwner == nullptr) {
        return world.getPuck();
    }*/
/*    if (world.getPuck().getOwnerPlayerId() == world.getMyPlayer().getId())
    {
        return getNearestOpponentCorner(self, world, (self.getY() - MIDDLE_BY_Y) * (world.getPuck().getY() - MIDDLE_BY_Y) > 0);
    }
    else
    {
        return getNearestOpponentCorner(self, world, false);
    }

    double x = MIDDLE_BY_X +
            (world.getOpponentPlayer().getNetFront() == world.getOpponentPlayer().getNetRight() ? -1 : 1) * DISTANCE_FOR_FLANK_X;
    return ((world.getPuck().getY() > MIDDLE_BY_Y)
    ? Point(x, BORDER_BOTTOM_Y + DISTANCE_TO_GOALNET_Y)
    : Point(x, BORDER_TOP_Y - DISTANCE_TO_GOALNET_Y));*/
//    if (Singleton::getInstance().puckPlaceById(self.getId()).getY() < MIDDLE_BY_Y)
    if (world.getPuck().getOwnerPlayerId() == world.getOpponentPlayer().getId()
            && fabs(world.getPuck().getX() - world.getMyPlayer().getNetFront()) < MIDDLE_BY_X)
    {
        if (world.getPuck().getY() < MIDDLE_BY_Y )
        {
            return Point(world.getMyPlayer().getNetFront() + (world.getMyPlayer().getNetFront() < MIDDLE_BY_X ? 1 : -1 ) * 2 * DISTANCE_TO_GOALNET_X, world.getMyPlayer().getNetTop() - DISTANCE_TO_GOALNET_Y);
        }
        else
        {
            return Point(world.getMyPlayer().getNetFront() + (world.getMyPlayer().getNetFront() < MIDDLE_BY_X ? 1 : -1 ) * 2 * DISTANCE_TO_GOALNET_X, world.getMyPlayer().getNetBottom() + DISTANCE_TO_GOALNET_Y);
        }
    }
    Point aim = getGoalnetDefensePlace(world);
    return Point(aim.getX() + (world.getMyPlayer().getNetFront() < MIDDLE_BY_X ? 1 : -1) * 1.5 * DISTANSE_FROM_MY_GOALNET_X,
            aim.getY());
}

double ::StrategyMethods::timeToSimple(const MyHockeyist &self, const model::Game &game, double pX, double pY, double pvX, double pvY)
{
    int countTicks = 0;
    double sX = self.getX();
    double sY = self.getY();
    double svX = self.getSpeedX();
    double svY = self.getSpeedY();
    double selfAngle = self.getAngle();
    while (countTicks < TICKS_FOR_PUCK)
    {
        Point nowSelf(self.getId(), self.getMass(), self.getRadius(), sX, sY, svX, svY, selfAngle, self.getAngularSpeed());
        double angleToUnit = nowSelf.getAngleTo(pX, pY);
        if (sqrt(sqr(sX - pX) + sqr(sY - pY)) < game.getStickLength()
                && fabs(angleToUnit) < game.getStickSector()/2.0
                && (self.getRemainingCooldownTicks() - countTicks <= 0)
                && (self.getRemainingKnockdownTicks() - countTicks <= 0))
        {
            break;
        }
        if (outOfPlace(pX, pY, PUCK_RADIUS))
        {
            simulatePuckAndWall(pX, pY, pvX, pvY, PUCK_RADIUS);
        }
        else
        {
            pX += pvX;
            pY += pvY;
        }
        decreaseSpeed(pvX, false);
        decreaseSpeed(pvY, false);
        if (angleToUnit < 0)
        {
            selfAngle -= min(fabs(angleToUnit), game.getHockeyistTurnAngleFactor() * self.getHockeyistTurnDereviation());
        }
        else
        {
            selfAngle += min(fabs(angleToUnit), game.getHockeyistTurnAngleFactor() * self.getHockeyistTurnDereviation());
        }
        setCorrectAngle(selfAngle);
        double speedCoef = ((fabs(angleToUnit) >= MAX_ANGLE_TO_SPEED_UP) ? SPEED_DOWN_HOCKEYIST_FACTOR : SPEED_UP_HOCKEYIST_FACTOR) * self.getHockeyistSpeedDereviation();
        svX += cos(selfAngle) * speedCoef;
        svY += sin(selfAngle) * speedCoef;
        decreaseSpeed(svX, true);
        decreaseSpeed(svY, true);
        sX += svX;
        sY += svY;
        countTicks++;
    }
    return countTicks;
}

double ::StrategyMethods::setCorrectAngle(double &angle)
{
    while (angle > M_PI) { angle -= 2 * M_PI; }
    while (angle < -M_PI) { angle += 2 * M_PI; }
    return angle;
}

int ::StrategyMethods::substituteIndex(const MyHockeyist &self, const model::World &world, const model::Game &game)
{
    double maxStamina = self.getStamina();
    int teammateIndex = -1;
    if (fabs(self.getY() - BORDER_TOP_Y) < game.getSubstitutionAreaHeight()
            && fabs(self.getX() - world.getMyPlayer().getNetFront()) < MIDDLE_BY_X)
    {
        for (auto hockeyist : world.getHockeyists())
        {
            if (hockeyist.isTeammate() && hockeyist.getType() != GOALIE
                    && hockeyist.getStamina() > maxStamina
                    && hockeyist.getState() == RESTING)
            {
                maxStamina = hockeyist.getStamina();
                teammateIndex = hockeyist.getTeammateIndex();
            }
        }
    }
    return (maxStamina - self.getStamina() > STAMINA_MIN_DELTA ? teammateIndex : -1);
}

double ::StrategyMethods::getCorrectAngle(const double angle)
{
    double angle2 = angle;
    while (angle2 > M_PI) { angle2 -= 2 * M_PI; }
    while (angle2 < -M_PI) { angle2 += 2 * M_PI; }
    return angle2;
}
