#include "MyHockeyist.h"

#ifndef MYDEFENSEHOCKEYIST_H
#define MYDEFENSEHOCKEYIST_H

class MyDefenseHockeyist : public MyHockeyist
{
public:
    MyDefenseHockeyist() : MyHockeyist() {};
    MyDefenseHockeyist(const model::Hockeyist& other) : MyHockeyist(other) {};

    bool takePuckToDefense(const model::World &world, const model::Game &game, model::Move &move) const;
    void moveToPuck(const model::World &world, const model::Game &game, model::Move &move);
    void defenseGoalNet(const model::World &world, const model::Game &game, model::Move &move);
    void moveToGoalNet(const model::World &world, const model::Game &game, model::Move &move);
    void strikeToGoalNet(const model::World &world, const model::Game &game, model::Move &move) const;
    bool solveTroubles(const model::World &world, const model::Game &game, model::Move &move);
};

#endif // MYDEFENSEHOCKEYIST_H
