#pragma once

#ifndef _MY_STRATEGY_H_
#define _MY_STRATEGY_H_


#include "Strategy.h"
#include "MyHockeyist.h"
#include "Point.h"
#include <cmath>
#include <fstream>
#include "Singleton.h"
#include "MyStrategyConst.h"
#include <iostream>

class MyStrategy : public Strategy {
    std::ofstream fout;
public:
    MyStrategy();

    void move(MyHockeyist& self, const model::World&world, const model::Game&game, model::Move& move);
    void move(const model::Hockeyist& self2, const model::World&world, const model::Game&game, model::Move& move);
};

#endif
