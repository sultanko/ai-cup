#ifndef MYHOCKEYIST_H
#define MYHOCKEYIST_H

#include <fstream>
#include "model/Hockeyist.h"
#include "Strategy.h"
#include "Point.h"
#include "MyStrategyConst.h"
#include "Singleton.h"
#include "Vec2d.h"


class MyHockeyist : public model::Hockeyist
{


public:
    MyHockeyist() : Hockeyist() {};
    MyHockeyist(const model::Hockeyist& other) :
            Hockeyist(other.getId(), other.getPlayerId(), other.getTeammateIndex(), other.getMass(), other.getRadius(),
            other.getX(), other.getY(), other.getSpeedX(), other.getSpeedY(), other.getAngle(), other.getAngularSpeed(),
            other.isTeammate(), other.getType(), other.getStrength(), other.getEndurance(), other.getDexterity(),
            other.getAgility(), other.getStamina(), other.getState(), other.getOriginalPositionIndex(),
            other.getRemainingKnockdownTicks(), other.getRemainingCooldownTicks(), other.getSwingTicks(),
            other.getLastAction(), other.getLastActionTick())
    {};

    double getPuckAngleDereviation() const;
    double getHockeyistTurnDereviation() const;
    double getHockeyistSpeedDereviation() const;
    double getPuckSpeedDereviation() const;
    double getOnStamina(double attribute) const;

    static void aimTo(const MyHockeyist &self, const model::World &world, const model::Game &game, model::Move &move, const Point &aim);
    static void moveTo(const MyHockeyist &self, const model::World &world, const model::Game &game, model::Move &move, const Point &aim, int backMove);
    static void moveTo(const MyHockeyist &self, const model::World &world, const model::Game &game, model::Move &move, const Point &aim, const Point &aim2, bool backMove);
    static void moveToSubstitute(const MyHockeyist& self, const model::World &world, const model::Game &game, model::Move &move);


    virtual bool takePuckToDefense(const model::World &world, const model::Game &game, model::Move &move) const = 0;
    virtual void moveToPuck(const model::World &world, const model::Game &game, model::Move &move) = 0;
    virtual void defenseGoalNet(const model::World &world, const model::Game &game, model::Move &move) = 0;
    virtual void moveToGoalNet(const model::World &world, const model::Game &game, model::Move &move) = 0;
    virtual void strikeToGoalNet(const model::World &world, const model::Game &game, model::Move &move) const = 0;
    virtual bool solveTroubles(const model::World &world, const model::Game &game, model::Move &move) = 0;

    virtual  ~MyHockeyist() {};
};

#endif // MYHOCKEYIST_H
