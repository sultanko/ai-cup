#include "MyHockeyist.h"

#ifndef MYAGRESSIVEHOCKEYIST_H
#define MYAGRESSIVEHOCKEYIST_H

class MyAgressiveHockeyist : public MyHockeyist
{
public:
    MyAgressiveHockeyist() : MyHockeyist() {};
    MyAgressiveHockeyist(const model::Hockeyist& other) : MyHockeyist(other) {};

    bool takePuckToDefense(const model::World &world, const model::Game &game, model::Move &move) const;
    void moveToPuck(const model::World &world, const model::Game &game, model::Move &move);
    void defenseGoalNet(const model::World &world, const model::Game &game, model::Move &move);
    void moveToGoalNet(const model::World &world, const model::Game &game, model::Move &move);
    void strikeToGoalNet(const model::World &world, const model::Game &game, model::Move &move) const;
    bool solveTroubles(const model::World &world, const model::Game &game, model::Move &move);
};

#endif // MYAGRESSIVEHOCKEYIST_H
