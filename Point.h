#ifndef POINT_H
#define POINT_H

#include "model/Unit.h"

class Point : public model::Unit
{
public:
    Point(long long int id, double mass, double radius, double x, double y, double speedX, double speedY, double angle, double angularSpeed)
            : Unit(id,mass,radius,x,y,speedX,speedY,angle,angularSpeed)
    {

    };
    Point(const Point& other) : Unit(other.getId(), other.getMass(), other.getRadius(), other.getX(), other.getY(), other.getSpeedX(), other.getSpeedY(), other.getAngle(), other.getAngularSpeed())
    {

    };
    Point();
    Point(const model::Unit&);
    Point(double x1, double y1);
    ~Point() {};
//    double getX() const { return x; }
//    double getY() const { return y; }
//    void setX(double x1) { x = x1; }
//    void setY(double y1) { y = y1; }
};

#endif // POINT_H
