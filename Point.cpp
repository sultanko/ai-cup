#include "Point.h"

Point::Point()
    : Unit(0, 0, 0, 0, 0, 0, 0, 0, 0)
{
}

Point::Point(const model::Unit& other)
        : Unit(other.getId(), other.getMass(), other.getRadius(), other.getX(), other.getY(), other.getSpeedX(), other.getSpeedY(), other.getAngle(), other.getAngularSpeed())
{
}

Point::Point(double x1, double y1)
    : Unit(0, 0, 0, x1, y1, 0, 0, 0, 0)
{
}

