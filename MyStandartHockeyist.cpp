#include <iostream>
#include "MyStandartHockeyist.h"
#include "StrategyMethods.h"
#include "MyAgressiveHockeyist.h"
#include "MyDefenseHockeyist.h"

using namespace MyStrategyConst;
using namespace model;
using namespace std;
using namespace MyHockeyistConst;
using namespace StrategyMethods;

bool MyStandartHockeyist::takePuckToDefense(const World &world, const Game &game, model::Move &move) const
{
    const Puck& puck = world.getPuck();
    if (getState() == SWINGING)
    {
        strikeToGoalNet(world, game, move);
        return true;
    }
    if (getRemainingKnockdownTicks() > 0 || getRemainingCooldownTicks() > 0)
    {
        return false;
    }
    if (world.getPuck().getOwnerPlayerId() == world.getMyPlayer().getId())
    {
        const Hockeyist* puckOwner = getPuckOwner(world);
        if (getDistanceTo(puckOwner->getX(), puckOwner->getY()) < game.getStickLength()
                && fabs(getAngleTo(puckOwner->getX(), puckOwner->getY())) < game.getStickSector()/2.0)
        {
            return false;
        }
        if (getDistanceTo(world.getPuck()) < game.getStickLength()
                && fabs(getAngleTo(world.getPuck())) < game.getStickSector()/2.0)
        {
            return false;
        }
    }

    for (auto hockeyist : world.getHockeyists())
    {
        if (isActiveHockeyist(hockeyist, false)
                && getDistanceTo(hockeyist) < game.getStickLength()
                && fabs(getAngleTo(hockeyist)) < game.getStickSector()/2.0)
        {
            move.setAction(ActionType::STRIKE);
            return true;
        }
    }

    if (fabs(getAngleTo(puck)) < game.getStickSector()/2.0
            && getDistanceTo(puck) < game.getStickLength())
    {
        if (isStrikeGetGoal(*this, world, game, false, 0, 0, true) >= CHANCE_PUCK_STRIKE_GOALIE)
        {
            return false;
        }
        if (isStrikeGetGoal(*this, world, game, false) > MIN_STRIKE_PROBABILILITY)
        {
            move.setAction(ActionType::STRIKE);
            return true;
        }
        if (puck.getOwnerPlayerId() != -1)
        {
            move.setAction(ActionType::STRIKE);
        }
        else
        {

            if ( (world.getMyPlayer().getNetFront() - world.getPuck().getX())  * (world.getPuck().getSpeedX()) > 0
                    && fabs(world.getPuck().getSpeedY()) > game.getGoalieMaxSpeed()
                    && fabs(world.getPuck().getX() - world.getMyPlayer().getNetFront()) < MIDDLE_BY_X)
            {
                double intersectByY = world.getPuck().getX() + world.getPuck().getSpeedY() *
                        (world.getMyPlayer().getNetFront() - world.getPuck().getX()) / world.getPuck().getSpeedX();
                double inetrsectByX = MIDDLE_BY_X;
                if ( (world.getMyPlayer().getNetTop() - world.getPuck().getY()) * world.getPuck().getSpeedY() > 0)
                {
                    inetrsectByX = world.getPuck().getX()
                            + world.getPuck().getSpeedX() *
                            (world.getMyPlayer().getNetTop() - world.getPuck().getY()) / world.getPuck().getSpeedY();
                }
                if ((world.getMyPlayer().getNetBottom() - world.getPuck().getY()) * world.getPuck().getSpeedY() > 0
                        && (world.getPuck().getY() < world.getMyPlayer().getNetBottom()))
                {
                    inetrsectByX = world.getPuck().getX()
                            + world.getPuck().getSpeedX() *
                            (world.getMyPlayer().getNetBottom() - world.getPuck().getY()) / world.getPuck().getSpeedY();
                }
                if ((inetrsectByX - world.getMyPlayer().getNetFront()) * (world.getMyPlayer().getNetBack() - world.getMyPlayer().getNetFront()) > 0
                        && intersectByY > world.getMyPlayer().getNetTop()
                        && intersectByY < world.getMyPlayer().getNetBottom())
                {
                    move.setAction(ActionType::STRIKE);
                }
                else
                {
                    move.setAction(TAKE_PUCK);
                }
            }
            else
            {
                move.setAction(ActionType::TAKE_PUCK);
            }
        }
        return true;
    }
    if (substituteIndex(*this, world, game) != -1 && getStamina() < STAMINA_CHANGE_COEF_IN_GAME * STAMINA_MAX_HOCKEYIST)
    {
        moveToSubstitute(*this, world, game, move);
        return true;
    }
    if (isStrikeGetGoal(*this, world, game, false) > MIN_STRIKE_PROBABILILITY)
    {
        Point aim = getPuckBestPlace(*this, world, game, world.getPuck().getX(), world.getPuck().getY(),
                world.getPuck().getSpeedX(), world.getPuck().getSpeedY(), true);
        if (getDistanceTo(aim) < game.getStickLength() && fabs(getAngleTo(aim)) < game.getStickSector()/2.0)
        {
            if (world.getPuck().getDistanceTo(aim)
                    / sqrt(sqr(world.getPuck().getSpeedX()) + sqr(world.getPuck().getSpeedY())) > game.getSwingActionCooldownTicks())
            {
                move.setAction(ActionType::SWING);
            }
            return true;
        }
    }
    if (puck.getOwnerHockeyistId() != -1 && puck.getOwnerPlayerId() != world.getMyPlayer().getId())
    {
        for (auto hockeyist : world.getHockeyists())
        {
            if (hockeyist.getId() == puck.getOwnerHockeyistId())
            {
                if (getDistanceTo(hockeyist) < game.getStickLength()
                        && fabs(getAngleTo(hockeyist)) < game.getStickSector()/2.0)
                {
                    move.setAction(ActionType::STRIKE);
                    return false;
                }
            }
        }
    }
    return false;
}

void MyStandartHockeyist::moveToPuck(const World& world, const Game& game, model::Move &move)
{
//    const Puck& puck = world.getPuck();
    // want to take control
    if (!takePuckToDefense(world, game, move))
    {
//        print_debug(cout, world.getTick(), getId(), getX(), getY(), getAngleTo(world.getPuck()));
        if (Singleton::getInstance().getHockeyistIdMinTime() == getId())
        {
//            print_debug(cout, world.getTick(), "Puck", getId());
            moveTo(*this, world, game, move, getPuckBestPlace(*this, world, game), 0);
            return;
        }
        if (Singleton::getInstance().getIdMinDefenseNoPuck() == getId())
        {
//            print_debug(cout, world.getTick(), "Defense", getId());
            int countNearest = 0;
            for (auto hockeyist : world.getHockeyists())
            {
                if (isActiveHockeyist(hockeyist, true) && hockeyist.getId() != getId()
                        && fabs(hockeyist.getX() - world.getMyPlayer().getNetFront()) < fabs(world.getPuck().getX() - world.getMyPlayer().getNetFront()))
                {
                    countNearest++;
                }
            }
            if (countNearest < 2 && fabs(Singleton::getInstance().puckPlaceById(getId()).getX() - world.getMyPlayer().getNetFront()) < MIDDLE_BY_X
                    && fabs(world.getPuck().getX() - world.getMyPlayer().getNetFront()) < MIDDLE_BY_X)
            {
                moveTo(*this, world, game, move, getPuckBestPlace(*this, world, game), 0);
                return;
            }
            MyDefenseHockeyist self2{*this};
            self2.defenseGoalNet(world, game, move);
            return;
        }
        if (getStamina() < STAMINA_CHANGE_COEF_IN_GAME * STAMINA_MAX_HOCKEYIST)
        {
//            print_debug(cout, world.getTick(), "Substitute", getId());
            moveToSubstitute(*this, world, game, move);
        }
        else
        {
//            print_debug(cout, world.getTick(), "Attack", getId());
            MyAgressiveHockeyist self2{*this};
            self2.moveToGoalNet(world, game, move);
            return;
        }
    }
}

void MyStandartHockeyist::defenseGoalNet(const World& world, const Game& game, model::Move &move)
{
    if (getId() != Singleton::getInstance().getIdMinDefenseNoPuck())
    {
        if (getStamina() < STAMINA_CHANGE_COEF_IN_GAME * STAMINA_MAX_HOCKEYIST)
        {
            moveToSubstitute(*this, world, game, move);
        }
        else
        {
            MyAgressiveHockeyist self2{*this};
            self2.moveToGoalNet(world, game, move);
        }
        return;
    }
    if (world.getPuck().getOwnerPlayerId() == world.getMyPlayer().getId())
    {
        for (auto hockeyist : world.getHockeyists())
        {
            if (isActiveHockeyist(hockeyist, false)
                    && fabs(hockeyist.getX() - world.getMyPlayer().getNetBack()) < fabs(getX() - world.getMyPlayer().getNetFront())
                    && fabs(hockeyist.getX() - world.getMyPlayer().getNetBack()) < MIDDLE_BY_X)
            {
                moveTo(*this, world, game, move, hockeyist, 0);
                if (getDistanceTo(hockeyist) < game.getStickLength()
                        && fabs(getAngleTo(hockeyist)) < game.getStickSector()/2.0)
                {
                    move.setAction(ActionType::STRIKE);
                }
                return;
            }
        }
    }
/*    if (world.getPuck().getOwnerPlayerId() == world.getMyPlayer().getId())
    {
        for (auto hockeyist : world.getHockeyists())
        {
            if (isActiveHockeyist(hockeyist, true)
                    && hockeyist.getId() != getId()
                    && hockeyist.getType() != HockeyistType::FORWARD
                    && hockeyist.getDistanceTo(getGoalnetDefensePlace(world)) < getDistanceTo(getGoalnetDefensePlace(world))
                    && hockeyist.getId() != world.getPuck().getOwnerHockeyistId()
                    )
            {
                if (getStamina() < STAMINA_CHANGE_COEF_IN_GAME * STAMINA_MAX_HOCKEYIST)
                {
                    moveToSubstitute(*this, world, game, move);
                }
                else
                {
                    MyAgressiveHockeyist self2{*this};
                    self2.moveToGoalNet(world, game, move);
                }
                return;
            }
        }
    }*/
    moveTo(*this, world, game, move, getGoalnetDefensePlace(world), world.getPuck(), true);
//    moveTo(*this, world, game, move, getGoalnetDefensePlace(world), Point(world.getMyPlayer().getNetFront(), MIDDLE_BY_Y), true);
}

void MyStandartHockeyist::strikeToGoalNet(const model::World &world, const model::Game &game, model::Move &move) const
{
    Point aim = getFarOpponentCorner(*this, world);
    if (getState() == HockeyistState::SWINGING)
    {
//        print_debug(cout, world.getTick(), "SWINGING: ", getX(), getY(), getAngle(), getAngularSpeed(), isStrikeGetGoal(*this, world, game, false));
        if (world.getPuck().getOwnerHockeyistId() != getId()
            && isStrikeGetGoal(*this, world, game, false) > MIN_SWINGING_STRIKE_PROBABILITY
                && getDistanceTo(world.getPuck()) < game.getStickLength()
                && fabs(getAngleTo(world.getPuck()) < game.getStickSector()/2.0))
        {
            move.setAction(ActionType::STRIKE);
        }
        else if (isStrikeGetGoal(*this, world, game, false) > MIN_STRIKE_PROBABILILITY
                && world.getPuck().getOwnerHockeyistId() == getId())
        {
           move.setAction(ActionType::STRIKE);
        }
        else if (getSwingTicks() >= game.getMaxEffectiveSwingTicks())
        {
//            print_debug(cout, world.getTick(), "SWINGING: ", getSwingTicks(), isStrikeGetGoal(*this, world, game, false));
            if (world.getPuck().getOwnerHockeyistId() == getId())
            {
                if (isStrikeGetGoal(*this, world, game, false) > MIN_SWINGING_STRIKE_PROBABILITY)
                {
                    move.setAction(STRIKE);
                }
                else
                {
                    move.setAction(CANCEL_STRIKE);
                }
                return;
            }
            Point aim = getPuckBestPlace(*this, world, game, world.getPuck().getX(), world.getPuck().getY(),
                    world.getPuck().getSpeedX(), world.getPuck().getSpeedY(), true);
            if (fabs(getAngleTo(aim)) < game.getStickSector()/2.0 && getDistanceTo(aim) < game.getStickLength())
            {
                return;
            }
            move.setAction(ActionType::CANCEL_STRIKE);
        }
        return;
    }
    if ( isStrikeGetGoal(*this, world, game, true) > MIN_STRIKE_PROBABILILITY)
    {
//        print_debug(cout, world.getTick(), "Good: ", isStrikeGetGoal(*this, world, game, true));
        move.setAction(ActionType::SWING);
    }
    else
    {
        move.setTurn(getAngleTo(aim));
    }
}

void MyStandartHockeyist::moveToGoalNet(const model::World &world, const model::Game &game, model::Move &move)
{
    bool puckOwner = getId() == world.getPuck().getOwnerHockeyistId();

    Point aim = getNearestOpponentCorner(*this, world, puckOwner);
    Point safeFlank = getSafeFlank(*this, world);

    if (puckOwner)
    {
        if ((game.getStruckPuckInitialSpeedFactor() * getPuckSpeedDereviation() < MIN_SPEED_FOR_STRIKE)
                || (game.getStrikeAngleDeviation() * getPuckAngleDereviation() > MAX_ANGLE_DEREVIATION_FOR_STRIKE)
//                || (getX() - world.getOpponentPlayer().getNetFront()) < DISTANSE_FROM_MY_GOALNET_X)
                )
        {
            if (solveTroubles(world, game, move))
            {
                return;
            }
            moveToSubstitute(*this, world, game, move);
            return;
        }
    }

    if (fabs(getX() - world.getOpponentPlayer().getNetFront()) > fabs(safeFlank.getX() - world.getOpponentPlayer().getNetFront()) && puckOwner
            && fabs(world.getPuck().getX() - safeFlank.getX()) > MAX_DISTANCE_TO_SPEED_UP
            )
    {
        moveTo(*this, world, game, move, safeFlank, getFarOpponentCorner(*this, world), false);
    }
    else
    {
/*        if (fabs(getX() - world.getOpponentPlayer().getNetFront()) < fabs(aim.getX() - world.getOpponentPlayer().getNetFront())
                || (getY() >= world.getOpponentPlayer().getNetTop()
                && getY() <= world.getOpponentPlayer().getNetBottom())
                || (fabs(getX() - safeFlank.getX()) < DISTANCE_FOR_FLANK_X)
                )
        {*/
            moveTo(*this, world, game, move, aim, getFarOpponentCorner(*this, world), true);
//            print_debug(cout, world.getTick(), "Nearest: ", getX(), getY(), world.getPuck().getX(), world.getPuck().getY(), aim.getX(), aim.getY());
/*        }
        else
        {
//            print_debug(cout, world.getTick(), "Far: ", getX(), getY(), world.getPuck().getX(), world.getPuck().getY());
            moveTo(*this, world, game, move, getFarOpponentCorner(*this, world), false);
        }*/
    }
    if (getState() != HockeyistState::SWINGING)
    {
/*        for (auto hockeyist : world.getHockeyists())
        {
            if (isActiveHockeyist(hockeyist, false)
                    && fabs(hockeyist.getAngleTo(world.getPuck())) < game.getStickSector()/2.0
                    && fabs(hockeyist.getX() - world.getOpponentPlayer().getNetFront()) < fabs(getX() - world.getOpponentPlayer().getNetFront()))
            {
                for (auto teammate : world.getHockeyists())
                {
                    if (isActiveHockeyist(teammate, true)
                            && fabs(getAngleTo(teammate.getX() + cos(teammate.getAngle()) * game.getPuckBindingRange(),
                            teammate.getY() + sin(teammate.getAngle()) * game.getPuckBindingRange()))
                            < game.getPassSector()/2.0)
                    {
                        MyStandartHockeyist self2{teammate};
                        if (isStrikeGetGoal(self2, world, game, true) > MIN_STRIKE_PROBABILILITY)
                        {
                            print_debug(cout, world.getTick(), "PASS_OD");
                            move.setAction(PASS);
                            move.setPassPower(PASS_PUCK_SPEED/
                                    (game.getStruckPuckInitialSpeedFactor()*game.getPassPowerFactor()*self2.getPuckSpeedDereviation()));
                            move.setPassAngle(getAngleTo(teammate.getX() + cos(teammate.getAngle()) * game.getPuckBindingRange(),
                                    teammate.getY() + sin(teammate.getAngle()) * game.getPuckBindingRange()));
                            return;
                        }
                    }
                }
            }
        }*/
        double anglePass;
        anglePass = deltaGoalByStrike(*this, world, game);
        if (fabs(anglePass) < DELTA_FOR_STRIKE)
        {
//            print_debug(cout, world.getTick(), anglePass);
            strikeToGoalNet(world, game, move);
            if (move.getAction() != SWING)
            {
                move.setTurn(anglePass);
                move.setSpeedUp(0);
            }
            else
            {
//                print_debug(cout, world.getTick(), "SWING: ", getX(), getY(), getAngle(), getAngularSpeed(), isStrikeGetGoal(*this, world, game, true));
            }
            return;
        }
        anglePass = strikeByPass(*this, world, game);
        if (fabs(anglePass) < game.getPassSector()/2.0)
        {
//            print_debug(cout, world.getTick(), anglePass, "PASS", isStrikeGetGoal(*this, world, game, false, anglePass, 0, false));
            move.setAction(ActionType::PASS);
            move.setPassAngle(anglePass);
            move.setPassPower(DEFAULT_PASS_POWER);
            return;
        }
    }
    else
    {
        strikeToGoalNet(world, game, move);
    }
}

bool MyStandartHockeyist::solveTroubles(const model::World &world, const model::Game &game, model::Move &move)
{
    double minEnemyTicks = INF;
    double minTeammateTicks = INF;
    double bestPassAngle = -game.getPassSector();
    double bestPassScore = 1.0;
    if (getRemainingCooldownTicks() != 0)
    {
        return false;
    }
    for (double passAngle = -game.getPassSector()/2.0; passAngle <= game.getPassSector()/2.0; passAngle += 2 * DELTA_FOR_ANGLE_VARIANTS)
    {
        for (auto hockeyist : world.getHockeyists())
        {
            if (isActiveHockeyist(hockeyist, false))
            {
                minEnemyTicks = min(minEnemyTicks,
                        timeToSimple(MyStandartHockeyist{hockeyist}, game,
                                world.getPuck().getX(), world.getPuck().getY(),
                                cos(getAngle() + passAngle) * PASS_PUCK_SPEED,
                                sin(getAngle() + passAngle) * PASS_PUCK_SPEED));
            }
            if (isActiveHockeyist(hockeyist, true) && hockeyist.getId() != getId())
            {
                minTeammateTicks = min(minTeammateTicks,
                        timeToSimple(MyStandartHockeyist{hockeyist}, game,
                                world.getPuck().getX(), world.getPuck().getY(),
                                cos(getAngle() + passAngle) * PASS_PUCK_SPEED,
                                sin(getAngle() + passAngle) * PASS_PUCK_SPEED));
            }
        }
        if (minEnemyTicks - minTeammateTicks > bestPassScore)
        {
            bestPassScore = minEnemyTicks - minTeammateTicks;
            bestPassAngle = passAngle;
        }
    }
    if (fabs(bestPassAngle) <= game.getPassSector()/2.0)
    {
        move.setAction(ActionType::PASS);
        move.setPassAngle(bestPassAngle);
        move.setPassPower(PASS_PUCK_SPEED / (getPuckSpeedDereviation() * game.getPassPowerFactor()*game.getStruckPuckInitialSpeedFactor()));
        return true;
    }
    if (isStrikeGetGoal(*this, world, game, false) > MIN_STRIKE_PANIC_PROBABILITY)
    {
        move.setAction(ActionType::STRIKE);
        return true;
    }
    return false;
}

